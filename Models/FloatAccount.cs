﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class FloatAccount
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public double AmountHeld { get; set; }
        public string UserID { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public FloatAccountType FloatAccountType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public List<FloatTransaction> FloatTransactions { get; set; }
    }

    public enum FloatAccountType
    {
        Primary, Secondary
    }
}