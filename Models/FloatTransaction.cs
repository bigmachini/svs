﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
   

    public class FloatTransaction
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public string Comments { get; set; }
        public EnumStatus Status { get; set; }
        public EnumTransactionType Type { get; set; }
        public DateTime DateAuthorized { get; set; }
        public DateTime? DateApproved { get; set; }
        public string InitiatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public int FloatAccountIn { get; set; }
        public int FloatAcountOut { get; set; }

    }

    public enum EnumStatus
    {
        Approved, Pending, Declined
    }

    public enum EnumTransactionType
    {
        Request, Transfer, Added
    }
}
