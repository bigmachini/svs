﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class ExpenditureTransaction
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public int FloatAccountID { get; set; }
        public ExpenditureStatus Status { get; set; }
        public DateTime DateInitiated { get; set; }
        public DateTime? DateApproved { get; set; }
        public string InitiatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public int ExpenditureTypeID { get; set; }
        public string Comment { get; set; }
        public ExpenditureType ExpenditureType{ get; set; }


    }

    public enum ExpenditureStatus
    {
        Approved, Declined, Pending
    }
}