﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Sales
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public decimal BuyingPrice { get; set; }
        public int AgentDetailID { get; set; }
        public string SalesAgentID { get; set; }
        public DateTime Timestamp { get; set; }
        [JsonIgnore]
        public virtual AgentDetails AgentDetail { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser SalesAgent { get; set; }
        [JsonIgnore]
        public virtual Product Product { get; set; }
        public virtual List<Device> Devices { get; set; }
    }

    public enum SaleStatus
    {

    }

}
