﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class ActionLog
    {
        public int ActionLogId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string IP { get; set; }
        public string UserNames { get; set; }
        public DateTime Timestamp { get; set; }
    }
}