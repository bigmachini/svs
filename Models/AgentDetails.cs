﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AgentDetails
    {
        public int ID { get; set; }
        public string AgentID { get; set; }
        public string Location { get; set; }
        public string AgentNumber { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public bool Active { get; set; }
        public Int32 UnitsInStock { get; set; }
        public Int32 ReorderLevel { get; set; }
        [JsonIgnore]
        public virtual ApplicationUser Agent { get; set; }
               [JsonIgnore]
        public virtual List<Sales> Sales { get; set; }
               [JsonIgnore]
        public virtual List<Certificate> Certificates { get; set; }
    }
}
