﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Device
    {
        public int ID { get; set; }
        public int? CertificateID { get; set; }
        public string SerialNumber { get; set; }
        public int SalesID { get; set; }
        public DeviceStatus status { get; set; }
        [JsonIgnore]
        public virtual Sales Sales { get; set; }
        [JsonIgnore]
        public virtual Certificate Certificate { get; set; }
    }

    public enum DeviceStatus
    {
        issued = 1, sold, returned
    }
}
