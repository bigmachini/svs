﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public  class TransactionStatus
    {
        public int ID { get; set; }
        public string InitiatedBy { get; set; }
        public DateTime DateInitiated { get; set; }
        public string FinalizeBy { get; set; }
        public DateTime? DateFinalized { get; set; }
        public String Comments { get; set; }
        public int? OrderID { get; set; }
        public int? FloatTransactionID { get; set; }
        public int? ExpenditureTransactionID { get; set; }
        public virtual Order Order { get; set; }
        public virtual FloatTransaction FloatTransaction { get; set; }
        public virtual ExpenditureTransaction ExpenditureTransaction { get; set; }
    }
}
