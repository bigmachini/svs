﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class OrderDetails
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public Decimal UnitPrice { get; set; }
        public double? Discount { get; set; }

        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }

       
    }
}
