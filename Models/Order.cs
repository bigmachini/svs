﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public enum OrderStatus
    {
        approved, pending, declined, saved,completed
    }
    public class Order
    {
        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? DateRequired { get; set; }
        public DateTime? DateShipped { get; set; }
        public int ShipVia { get; set; }
        public Decimal? Freight { get; set; }
        public String ShipperName { get; set; }
        public String ShipperAddress { get; set; }
        public String ShipperPostalCode { get; set; }
        public String ShipperCountry { get; set; }

        public virtual List<OrderDetails> OrderDetails { get; set; }
    }
}
