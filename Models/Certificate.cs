﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public enum CertificateStatus
    {
        notissued = 1, assigned, pending, issued
    }
    public class Certificate
    {
        public Int32 ID { get; set; }
        public string CertificateSerialNo { get; set; }
        public string CertificateNo {get; set;}
        public DateTime? DateIssued { get; set; }
        public CertificateStatus Status { get; set; }
        public int? AgentID { get; set; }
        public virtual AgentDetails Agent { get; set; }

        public Int32 GetCertificateSerialNo()
        {
            string certNo = CertificateSerialNo.Substring(2,7);
            return Int32.Parse(certNo);
        }

        public void SetCertificateSerialNo(int certNo)
        {
            CertificateSerialNo = String.Format("HD{0}", certNo);
        }

        public void GenerateCertificateNumber()
        {
           var hashCode = DateTime.Now.GetHashCode();
           var serialNo = GetCertificateSerialNo();
           if (hashCode < 0)
           {
               hashCode = hashCode * new Random().Next(111, 999) / 100;
           }
           var sCertNo = (hashCode + serialNo).ToString();
           CertificateNo = sCertNo;
        }
    }
}
