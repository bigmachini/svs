﻿using Dal;
using RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels;
using RaindropManagementSystem.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Helper
{
    public class AdminDB
    {
        /// <summary>
        /// Returns the status of the float account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static AdminFloatStatus CheckFloatAccount(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(id);
            AdminFloatStatus afsvm = new AdminFloatStatus()
                {
                    Amount = floatAccount.Amount + floatAccount.AmountHeld,
                    Status = floatAccount.Status
                };


            return afsvm;
        }

        public static AdminApproveViewModel CreateApproveModel(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var expenditureTransaction = db.ExpenditureTransactions.Find(id);
            AdminApproveViewModel aavm = new AdminApproveViewModel()
           {
               Amount = expenditureTransaction.Amount,
               DateInitiated = expenditureTransaction.DateInitiated,
               Description = expenditureTransaction.Description,
               ExpenditureTypeID = expenditureTransaction.ExpenditureTypeID,
               ID = expenditureTransaction.ID,
               Status = expenditureTransaction.Status,

           };

            var floatAccount = db.FloatAccounts.Find(expenditureTransaction.FloatAccountID);
            aavm.AccountName = floatAccount.Name;

            var user = db.Users.ToList().Where(x => x.Id == floatAccount.UserID).SingleOrDefault();
            aavm.UserNames = user.FullNames;

            return aavm;
        }
    }
}