﻿
using Dal;
using Models;
using RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Helper
{
    public class ExpenditureDB
    {
        public static void RaiseRequest(ExpenditureTransaction transaction)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(transaction.FloatAccountID);
            floatAccount.AmountHeld -= transaction.Amount;
            db.Entry(floatAccount).State = EntityState.Modified;
            db.SaveChanges();
        }

        public static void ApproveRequest(ExpenditureTransaction transaction)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(transaction.FloatAccountID);
            floatAccount.AmountHeld += transaction.Amount;
            floatAccount.Amount -= transaction.Amount;
            db.Entry(floatAccount).State = EntityState.Modified;
            db.SaveChanges();
        }

        public static void ReverseRequest(ExpenditureTransaction transaction)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(transaction.FloatAccountID);
            floatAccount.AmountHeld -= transaction.Amount;
            floatAccount.AmountHeld += transaction.Amount;
            db.Entry(floatAccount).State = EntityState.Modified;
            db.SaveChanges();
        }

        public static void DeclineRequest(ExpenditureTransaction transaction)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(transaction.FloatAccountID);
            floatAccount.AmountHeld += transaction.Amount;
            db.Entry(floatAccount).State = EntityState.Modified;
            db.SaveChanges();
        }

        public static List<ExpenditureTransactionVM> GetTransactions(List<ExpenditureTransaction> transactions)
        {
            List<ExpenditureTransactionVM> result = new List<ExpenditureTransactionVM>();
            foreach (var item in transactions)
            {
                ExpenditureTransactionVM data = new ExpenditureTransactionVM()
                {
                    Amount = item.Amount,
                    DateApproved = item.DateApproved,
                    DateInitiated = item.DateInitiated,
                    Description = item.Description,
                    ExpenditureType = item.ExpenditureType,
                    ID = item.ID,
                    Status = item.Status
                };
                data.InitiatedBy = FloatAdminDB.GetUserName(item.InitiatedBy);

                if (!string.IsNullOrWhiteSpace(item.ApprovedBy))
                    data.ApprovedBy = FloatAdminDB.GetUserName(item.ApprovedBy);

                result.Add(data);
            }

            return result;

        }

        internal static ExpenditureDetailVM GetTransactionDetail(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var expenditure = db.ExpenditureTransactions.Include(x => x.ExpenditureType).FirstOrDefault();

            ExpenditureDetailVM detail = new ExpenditureDetailVM()
            {
                Amount = expenditure.Amount,
                Comment = expenditure.Comment,
                DateInitiated = expenditure.DateInitiated,
                Description = expenditure.Description,
                ExpenditureType = expenditure.ExpenditureType,
                ID = expenditure.ID,
            };

            detail.InitiatedBy = FloatAdminDB.GetUserName(expenditure.InitiatedBy);
            if (!string.IsNullOrWhiteSpace(expenditure.ApprovedBy))
            {
                detail.DateApproved = expenditure.DateApproved;
                detail.ApprovedBy = FloatAdminDB.GetUserName(expenditure.ApprovedBy);
            }

            return detail;

        }

    }
}