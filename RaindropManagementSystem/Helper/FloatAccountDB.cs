﻿using Dal;
using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RaindropManagementSystem.Helper
{
    public class FloatAccountDB
    {
        /// <summary>
        /// This method is used to add funds to a float account
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public static bool AddFunds(int id, double amount)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                FloatAccount floatAccount = db.FloatAccounts.Find(id);
                double amountHeld = floatAccount.AmountHeld;
                floatAccount.AmountHeld = amountHeld + amount;
                db.Entry(floatAccount).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }


        /// <summary>
        /// This method removes funds from a particular float account 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public static bool RemoveFunds(int id, double amount)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var floatAccount = db.FloatAccounts.Find(id);
                double amountHeld = floatAccount.AmountHeld;
                floatAccount.AmountHeld = amountHeld - amount;
                db.Entry(floatAccount).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }

        /// <summary>
        /// This method is used to approve transactions.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public static bool ApproveFunds(FloatTransaction transaction)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var floatAccountIn = db.FloatAccounts.Find(transaction.FloatAccountIn);
                var floatAccountOut = db.FloatAccounts.Find(transaction.FloatAcountOut);

                double amountHeld = floatAccountIn.AmountHeld;
                floatAccountIn.Amount += transaction.Amount;
                floatAccountIn.AmountHeld = amountHeld - transaction.Amount;

                amountHeld = floatAccountOut.AmountHeld;
                floatAccountOut.Amount -= transaction.Amount;
                floatAccountOut.AmountHeld = amountHeld + transaction.Amount;

                db.Entry(floatAccountIn).State = EntityState.Modified;
                db.Entry(floatAccountOut).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }


        /// <summary>
        /// This method is used to approve transactions.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public static bool TransferFunds(FloatTransaction transaction)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var floatAccountIn = db.FloatAccounts.Find(transaction.FloatAccountIn);
                var floatAccountOut = db.FloatAccounts.Find(transaction.FloatAcountOut);

                double amountHeld = floatAccountIn.AmountHeld;
                floatAccountIn.AmountHeld = amountHeld + transaction.Amount;

                amountHeld = floatAccountOut.AmountHeld;
                floatAccountOut.AmountHeld = amountHeld - transaction.Amount;

                db.Entry(floatAccountIn).State = EntityState.Modified;
                db.Entry(floatAccountOut).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }

        /// <summary>
        /// This method is used to approve transactions.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="amount"></param>
        public static bool DeclineFunds(FloatTransaction transaction)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var floatAccountIn = db.FloatAccounts.Find(transaction.FloatAccountIn);
                var floatAccountOut = db.FloatAccounts.Find(transaction.FloatAcountOut);

                double amountHeld = floatAccountIn.AmountHeld;
                floatAccountIn.AmountHeld = amountHeld - transaction.Amount;

                amountHeld = floatAccountOut.AmountHeld;
                floatAccountOut.AmountHeld = amountHeld + transaction.Amount;

                db.Entry(floatAccountIn).State = EntityState.Modified;
                db.Entry(floatAccountOut).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }

        internal static bool AddFundsSuper(int id, double amount)
        {
            bool status = true;
            try
            {
                ApplicationDbContext db = new ApplicationDbContext();
                var floatAccount = db.FloatAccounts.Find(id);
                double previousAmount = floatAccount.Amount;
                floatAccount.Amount = previousAmount + amount;
                db.Entry(floatAccount).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
            catch
            {
                status = false;
            }

            return status;
        }

        public static bool CanTransact(int id, double amount)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var floatAccount = db.FloatAccounts.Find(id);
            double availableFund = floatAccount.Amount;

            if (floatAccount.AmountHeld < 0)
            {
                availableFund = floatAccount.Amount + floatAccount.AmountHeld;
            }
           

            if (availableFund < amount)
            {
                return false;
            }

            return true;
        }
    }
}