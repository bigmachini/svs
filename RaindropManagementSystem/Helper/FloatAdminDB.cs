﻿using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using Models;
using Dal;

namespace RaindropManagementSystem.Helper
{
    public class FloatAdminDB
    {

        public static async Task<List<FloatTransaction>> GetTransactions()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return await db.FloatTransactions.ToListAsync();
        }

        public static List<FloatAdminRequestVM> GetRequests(List<FloatTransaction> list, string userId)
        {
            List<FloatAdminRequestVM> farvm = new List<FloatAdminRequestVM>();
            int count = 0;
            if (!string.IsNullOrWhiteSpace(userId))
            {
                list = list.Where(x => x.Type == EnumTransactionType.Request && (x.InitiatedBy == userId || x.ApprovedBy == userId)).ToList();
                count = list.Count();
            }
            else
            {
                list = list.Where(x => x.Type == EnumTransactionType.Request).ToList();
                count = list.Count();
            }

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    FloatAdminRequestVM data = new FloatAdminRequestVM()
                    {
                        ID = list[i].ID,
                        Amount = list[i].Amount,
                        DateInitiated = list[i].DateAuthorized,
                        Description = list[i].Description,
                        Status = list[i].Status,
                    };

                    data.InitiatedBy = GetUserName(list[i].InitiatedBy);

                    farvm.Add(data);

                }
            }
            return farvm;
        }

        internal static List<FloatTransactionVM> GetPending(List<FloatTransaction> list, string userId)
        {
            int count = 0;
            List<FloatAdminRequestVM> farvm = new List<FloatAdminRequestVM>();
            List<FloatTransactionVM> ftvm= new List<FloatTransactionVM>();

            if(!string.IsNullOrWhiteSpace(userId))
            {

                list = list.Where(x => x.Status == EnumStatus.Pending && x.InitiatedBy != userId).ToList();
                count = list.Count();
            }
            else
            {
                list = list.Where(x => x.Status == EnumStatus.Pending ).ToList();
                count = list.Count();
            }


            if (count > 0)
            {


              ftvm =  GetTransactionView(list);
                for (int i = 0; i < count; i++)
                {
                    FloatAdminRequestVM data = new FloatAdminRequestVM()
                    {
                        ID = list[i].ID,
                        Amount = list[i].Amount,
                        DateInitiated = list[i].DateAuthorized,
                        Description = list[i].Description,
                        Status = list[i].Status,
                        Type = list[i].Type
                    };

                    data.InitiatedBy = GetUserName(list[i].InitiatedBy);
                    farvm.Add(data);
                }
            //if (count > 0)
            //{
          


            //    for (int i = 0; i < count; i++)
            //    {
            //        FloatAdminRequestVM data = new FloatAdminRequestVM()
            //        {
            //            ID = list[i].ID,
            //            Amount = list[i].Amount,
            //            DateInitiated = list[i].DateAuthorized,
            //            Description = list[i].Description,
            //            Status = list[i].Status,
            //            Type = list[i].Type
            //        };

            //        data.InitiatedBy = GetUserName(list[i].InitiatedBy);
            //        farvm.Add(data);
            //    }
            }

            return ftvm;
        }

        public static string GetUserName(string userId)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var user = db.Users.ToList().Where(x => x.Id == userId).SingleOrDefault();
            return user.FullNames;
        }

        internal static async Task<FloatTransaction> GetTransactionById(int id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            return await db.FloatTransactions.FindAsync(id);
        }

        internal static List<FloatTransactionVM> GetTransactionView(List<FloatTransaction> transactions)
        {
            int count = transactions.Count;
            List<FloatTransactionVM> result = new List<FloatTransactionVM>();

            if (count > 0)
            {
                foreach (var item in transactions)
                {
                    FloatTransactionVM data = new FloatTransactionVM()
                    {
                        Amount = item.Amount,
                        DateApproved = item.DateApproved,
                        DateInitiated = item.DateAuthorized,
                        Description = item.Description,
                        ID = item.ID,
                        Status = item.Status,
                        Type = item.Type
                    };

                    data.InitiatedBy = GetUserName(item.InitiatedBy);
                    if (!string.IsNullOrWhiteSpace(item.ApprovedBy))
                    {
                        data.ApprovedBy = GetUserName(item.ApprovedBy);
                    }

                    result.Add(data);

                }
            }
            return result;
        }
    }
}