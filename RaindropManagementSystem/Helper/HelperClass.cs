﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RaindropManagementSystem.Helper
{
    public class HelperClass
    {
        public static DateTime DateTimeNow()
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo EAZone = TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(timeUtc, EAZone);
        }

    }

    //This will hold all the return from the controllers
    public class ErrorClass
    {
        public List<String> ErrorMessages { get; set; }
        public ErrorClass()
        {
            this.ErrorMessages = new List<string>();
        }
    }


    //used to verify is data is valid or not
    public class ClientData
    {
        public bool  Status { get; set; }
        public Object Data { get; set; }
    }

}