﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RaindropManagementSystem.Startup))]
namespace RaindropManagementSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
