﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Generics
{
    public class AccountResult<T>
    {
        public AccountResult()
        {
            AccountType = "SuperAdmin";
            Accounts = new List<T>();
        }

        public AccountResult(string type, List<T> list)
        {
            this.AccountType = type;
            this.Accounts = list;
        }

        public string AccountType { get; set; }
        public List<T> Accounts { get; set; }
    }

    public class TransactionResult<T>
    {
        public TransactionResult()
        {
            UserId = "";
            Transactions = new List<T>();
        }

        public TransactionResult(string userId, List<T> list, string accountType = "")
        {
            this.UserId = userId;
            this.Transactions = list;
            this.AccountType = accountType;
        }

        public string UserId { get; set; }
        public string AccountType { get; private set; }
        public List<T> Transactions { get; set; }
    }
}