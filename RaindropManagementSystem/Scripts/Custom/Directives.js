﻿checkAgent = function ($http) {
    return {
        require: 'ngModel',
        link: function (scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function () {
                $http({
                    method: 'POST',
                    url: '/AgentDetail/SearchAgent/',
                    data: { 'agentNumber': attrs.checkAgent }
                }).success(function (data, status, headers, cfg) {
                    c.$setValidity('unique', data);
                }).error(function (data, status, headers, cfg) {
                    c.$setValidity('unique', false);
                });
            });
        }
    }
};

checkIfNumber = function () {
    return {
        require: 'ngModel',
        link: function (scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function () {
                var number = parseInt(attrs.checkIfNumber);
                if (typeof (number) === "number") {
                    c.$setValidity('number', number);
                } else {
                    c.$setValidity('number', false);
                }
            });
        }
    }
};

checkQuantity = function () {
    return {
        require: 'ngModel',
        link: function (scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function () {
                if (attrs.checkQuantity !== "NaN") {
                    var number = parseInt(attrs.checkQuantity);
                    if (number >= 0) {
                        c.$setValidity('units', true);
                    } else {
                        c.$setValidity('units', false);
                    }
                } else {
                    c.$setValidity('units', true);
                }
            });
        }
    }
};


