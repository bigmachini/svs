﻿function ProductCtrl($scope, $http, $location) {

    $scope.Product = {
        "ID": "",
        "Name": "",
        "Description": "",
        "UnitPrice": "",
        "UnitsInStock": "",
        "ReorderLevel": "",
        "UnitsOnOrder": "",
        "Discontinued": "",
        "Action": ""
    }
    $scope.showEdit = { value: false }

    $scope.ProductCreate =
        {
            "Name": "",
            "Description": "",
            "UnitPrice": 0,
            "UnitsInStock": 0,
            "ReorderLevel": 0,
            "UnitsOnOrder": 0,
        }

    $scope.User = {
        "AccountType": "",
        "UserId": "",
    }

    $scope.ProductEdit = {
        "ID": "",
        "Description": "",
        "ReorderLevel": ""
    }

    $scope.showError = { show: false };
    $scope.mySelections = [];
    $scope.Products = [];
    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        columnDefs: [{ field: 'Name', displayName: 'Product Name' },
                         { field: 'Description', displayName: 'Description' },
                         { field: 'UnitPrice', displayName: "Unit Price" },
                         { field: 'UnitsInStock', displayName: 'Units in Stock' },
                         { field: 'Discontinued', displayName: 'Discontinued' }
        ],
    };



    $scope.CreateProduct = function () {
        $http({ method: "POST", data: $scope.ProductCreate, url: "Create" }).
             success(function (data, status, headers, config) {

                 if (data.Status) {
                     $scope.ProductCreate = {};
                     alert("Product has been created");
                     $location.path('/');
                 }
                 else {
                     $scope.showError.show = true;
                     $scope.Errors = data.Data.ErrorMessages;
                 }

             });

        $scope.$apply();

    }

    $scope.EditProduct = function () {
        $scope.ProductEdit = {
            "ID": $scope.mySelections[0].ID,
            "Description": $scope.ProductEdit.Description,
            "ReorderLevel": $scope.ProductEdit.ReorderLevel
        }


        $http({ method: "POST", data: $scope.ProductEdit, url: "Edit" }).
        success(function (data, status, headers, config) {

            if (data.Status) {
                $scope.mySelections[0].Description = $scope.ProductEdit.Description;
                $scope.mySelections[0].ReorderLevel = $scope.ProductEdit.ReorderLevel;
                $scope.ProductEdit = {};
                alert("Product has been Modified");
                $scope.HideDetails();
            }
            else {
                $scope.showError.show = true;
                $scope.Errors = data.Data.ErrorMessages;
            }
        });
    }

    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                $http({ method: "GET", url: "GetProducts" }).
                    success(function (data, status, headers, config) {

                        $scope.User.AccountType = data.AccountType;
                        $scope.User.UserId = data.UserId;
                        $scope.Products = data.Transactions.filter(function (item) {
                            return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;

                        });

                        $scope.setPagingData($scope.Products, page, pageSize);
                    });
            } else {

                $http({ method: "GET", url: "GetProducts" }).
                    success(function (data, status, headers, config) {
                        $scope.User.AccountType = data.AccountType;
                        $scope.User.UserId = data.UserId;
                        $scope.Products = data.Transactions;

                        $scope.setPagingData($scope.Products, page, pageSize);
                    });
            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);


    $scope.HideDetails = function () {
        $scope.mySelections[0].__ng_selected__ = false;
        $scope.showEdit.value = false;
        $scope.Errors = {};
    }

    $scope.ShowEdit = function () {
        $scope.showEdit.value = true;
        $scope.ProductEdit.Description = $scope.mySelections[0].Description;
        $scope.ProductEdit.ReorderLevel = $scope.mySelections[0].ReorderLevel;
    }

}

function AgentCtrl($scope, $http) {

    $scope.Agent = {
        "ID": "",
        "FirstName": "",
        "MiddleName": "",
        "LastName": "",
        "IDNumber": "",
        "PhoneNumber": "",
        "Email": "",
        "Username": "",
        "Password": "",
        "ConfirmPassword": "",
        "CompanyName": "",
        "Location": "",
        "AgentNumber": "",
        "PhoneNumber2": "",
        "Active": true
    }

    $scope.AgentDetail =
        {
            "AgentNames": "",
            "CompanyName": "",
            "Location": "",
            "AgentNumber": "",
            "PhoneNumber": "",
            "UnitsInStock": "",
            "ReorderLevel": "",
            "Active": ""
        }

    $scope.PasswordRegex = /^(?=.*\d)(?=.*[a-zA-Z]).{6,20}$/;

    $scope.showCreate = { value: false }
    $scope.showAddDetails = { value: false }
    $scope.showDetails = { value: false }

    $scope.showError = { show: false };

    $scope.mySelections = [];
    $scope.AgentDetails = [];
    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };

    $scope.gridOptions = {

        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        enableHighlighting: false,
        columnDefs: [{ field: 'FirstName', displayName: 'First Name' },
                     { field: 'LastName', displayName: 'Last Name' },
                     { field: 'CompanyName', displayName: 'Company Name' },
                     { field: 'Location', displayName: "Location" },
                     { field: 'AgentNumber', displayName: 'Agent Number' },
                     { field: 'Active', displayName: 'Status' }
        ],
    };

    $scope.CreateAgent = function () {
        $scope.Agent.Active = true;
        alert("Company name " + $scope.Agent.CompanyName + " \n Agent Number " + $scope.Agent.AgentNumber)
        $http({ method: "POST", data: $scope.Agent, url: "/AgentDetail/Create" }).
        success(function (data, status) {

        })
    };

    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                $http({ method: "GET", url: "GetAgentDetails" }).
                    success(function (data, status, headers, config) {
                        //   $scope.User.AccountType = data.AccountType;
                        // $scope.User.UserId = data.UserId;
                        $scope.AgentDetails = data.filter(function (item) {
                            return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;

                        });

                        $scope.setPagingData($scope.AgentDetails, page, pageSize);
                    });
            } else {

                $http({ method: "GET", url: "GetAgentDetails" }).
                    success(function (data, status, headers, config) {
                        // $scope.User.AccountType = data.AccountType;
                        //$scope.User.UserId = data.UserId;
                        $scope.AgentDetails = data;
                        $scope.setPagingData($scope.AgentDetails, page, pageSize);
                    });
            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.$watch('mySelections[0].__ng_selected__', function (input) {
        if (input == true) {
            $http({ method: "get", url: "/agentdetail/getdetails/" + $scope.mySelections[0].ID })
            .success(function (data, status, headers) {
                $scope.AgentDetail = data;
                $scope.Hide();
                $scope.showDetails.value = true;
            });
        }
    });

    $scope.reset = function () {
        $scope.Hide();
        $scope.Agent = {};

    }

    $scope.Hide = function () {
        $scope.showCreate.value = false;
        $scope.showAddDetails.value = false;
        $scope.showDetails.value = false;
        if (typeof ($scope.mySelections[0]) !== 'undefined') {
            $scope.mySelections[0].__ng_selected__ = false;
        }
    }

    $scope.ShowCreate = function () {
        $scope.Hide();
        $scope.showCreate.value = true;
    }

    $scope.Next = function () {
        $scope.Hide();
        $scope.showAddDetails.value = true;
    }

    $scope.Back = function () {
        $scope.Hide();
        $scope.Agent.ConfirmPassword = "";
        $scope.showCreate.value = true;
    }

}

function SalesCtrl($scope, $http) {
    $scope.Sale = {
        "ID": "",
        "ProductID": "",
        "ProductName": "",
        "Quantity": 0,
        "BuyingPrice": 0.0,
        "Total": 0,
        "AgentID": "",
        "AgentName": ""
    };

    $scope.SaleItem =
      {
          "ID": "",
          "ProductName": "",
          "Quantity": 0,
          "BuyingPrice": 0.0,
          "Total": 0,
          "CompanyName": "",
          "SoldBy": "",
          "Timestamp": ""
      }

    $scope.Sales = [];
    $scope.showAddDevice = { value: false };

    $scope.product = {};
    $scope.agent = {};
    $scope.agents = { value: [] };
    $scope.products = { value: [] };
    $scope.Quantity = 0;
    $scope.Total = 0;
    $scope.UnitsInStock = 0;
    $scope.disableQuantity = { value: false };

    $scope.DeviceList =
        {
            "SerialNumbers": [],
            "Quantity": 0
        };



    $scope.$watch("Quantity", function () {
        var price = $scope.product.UnitPrice;
        if (typeof (price) == 'undefined') {
            $scope.Total = $scope.Quantity * 0;
        }

        else {

            if (typeof (parseInt($scope.Quantity)) !== "number") {
                $scope.Total = 0;
            }
            else if ($scope.Quantity > $scope.product.UnitsInStock) {
                $scope.UnitsInStock = $scope.product.UnitsInStock;
                $scope.Total = -1;
            }
            else {
                $scope.Quantity = parseInt($scope.Quantity);
                $scope.Total = $scope.Quantity * $scope.product.UnitPrice;
            }
        }
    });

    $scope.Load = function () {
        $scope.disableQuantity.value = false;
        $http({ method: "get", url: "/Sales/getdropdown" }).
        success(function (data, status, headers, config) {

            for (var i = 0; i < data.jsonProducts.length; i++) {
                var jp = data.jsonProducts[i];
                $scope.products.value.push(jp); //new ProductsVM(jp.ID, jp.Name, jp.UnitPrice));
            }

            for (var i = 0; i < data.jsonAgents.length; i++) {
                var et = data.jsonAgents[i];
                $scope.agents.value.push(et);
            }
            $scope.product = $scope.products.value[0];
            $scope.agent = $scope.agents.value[0];
        });

    }

    $scope.ProcessSale = function () {
        $scope.SaleVM =
            {
                "ProductID": $scope.product.ID,
                "Quantity": $scope.Quantity,
                "BuyingPrice": $scope.product.UnitPrice,
                "AgentID": $scope.agent.ID,
                "Devices": $scope.DeviceList.SerialNumbers
            }


        $http({ method: "post", data: $scope.SaleVM, url: "/SalesManagement/Sales/ProcessSale" })
        .success(function (data, status, header, config) {
            $scope.SaleItem = data;
            $scope.DeviceList.Quantity = $scope.Quantity;
            $scope.DeviceList.SaleID = data.ID;
            $scope.Quantity = 0;
            $scope.DeviceList.AgentID = $scope.agent.ID;
            $scope.agents = { value: [] };
            $scope.products = { value: [] };
            $scope.DeviceList = {};
            $scope.SerialNumber = {};
            $scope.showAddDevice.value = false;
            $scope.Load();
            alert("Sale successful posted");
        });
    }

    $scope.AssignDevices = function () {
        $scope.disableQuantity.value = true;
        $scope.showAddDevice.value = true;
        $scope.DeviceList.Quantity = $scope.Quantity;
        //$http({ method: "post", data: $scope.DeviceList, url: "/Sales/AssignDevices" })
        //.success(function (data, status, header, config) {
        //    alert("status: " + status);
        //    $scope.showAddDevice.value = false;
        //    $scope.DeviceList = {};
        //});
    }

    $scope.AddDevice = function () {

        $scope.DeviceList.SerialNumbers.push($scope.SerialNumber);
        $scope.DeviceList.Quantity -= 1;
    };

    $scope.Load();


}

function TransactionCtrl($scope, $http, $filter) {

    $scope.SaleListModel =
       {
           "ID": "",
           "ProductName": "",
           "Quantity": "",
           "BuyingPrice": "",
           "Total": "",
           "CompanyName": "",
           "SoldBy ": "",
           "Timestamp": ""
       }


    $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };


    $scope.Search = function setFilterText(value) {
        $scope.filterOptions.filterText = value;  //+ ";";//Type:' + $scope.filterCity + ";";
    }

    $scope.Reset = function setFilterText() {
        $scope.filterOptions.filterText = "";  //+ ";";//Type:' + $scope.filterCity + ";";
        $scope.filterName = '';
    }

    $scope.mySelections = [];
    $scope.Transactions = [];
    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                $http({ method: "get", url: "/Sales/GetTransactions" })
            .success(function (data, status, header, config) {
                $scope.setPagingData(data, page, pageSize);

                var search = data.Transactions.filter(function (item) {
                    return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;

                });

                $scope.setPagingData(search, page, pageSize);
            });
            } else {

                $http({ method: "get", url: "/Sales/GetTransactions" })
              .success(function (data, status, header, config) {
                  $scope.setPagingData(data, page, pageSize);
              });
            }
        }, 100);
    };
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        columnDefs: [{ field: 'ProductName', displayName: 'Product Name' },
                         { field: 'Quantity', displayName: 'Quantity' },
                         { field: 'BuyingPrice', displayName: 'Buying Price' },
                         { field: 'Total', displayName: 'Total' },
                         { field: 'SoldBy', displayName: 'Sold By' },
                         { field: 'Timestamp', displayName: 'Time Stamp' },
                         { field: 'CompanyName', displayName: 'Company Name' }],
    };
}

function DeviceCtrl($scope, $http, $filter) {
    Device = {
        ID: "",
        AgentCompany: "",
        DeviceSn: "",
        Status: "",
        CertificateNo: "",
    }

    $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };


    $scope.Search = function setFilterText(value) {
        $scope.filterOptions.filterText = value;  //+ ";";//Type:' + scope.filterCity + ";";
    }

    $scope.Reset = function setFilterText() {
        $scope.filterOptions.filterText = "";  //+ ";";//Type:' + scope.filterCity + ";";
        $scope.filterName = '';
    }

    $scope.mySelections = [];
    $scope.Devices = [];
    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150, 200],
        pageSize: 50,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                $http({ method: "get", url: "/Devices/GetDevices" })
            .success(function (data, status, header, config) {
                $scope.setPagingData(data, page, pageSize);

                var search = data.filter(function (item) {
                    return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;

                });

                $scope.setPagingData(search, page, pageSize);
            });
            } else {

                $http({ method: "get", url: "/Devices/GetDevices" })
              .success(function (data, status, header, config) {
                  $scope.setPagingData(data, page, pageSize);
              });
            }
        }, 100);
    };
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.DeviceDetail = {
        AgentName: "",
        AgentCompany: "",
        SerialNumber: "",
        Status: "",
        CertificateNo: "",
        SoldBy: "",
        DateSold: "",
        DateInstalled: ""
    }

    $scope.$watch('gridOptions.selectedItems', function () {

        if (typeof ($scope.gridOptions.selectedItems) !== "undefined" && $scope.gridOptions.selectedItems.length > 0) {
            // console.log("length of scope.mySelection is " + $scope.gridOptions.selectedItems[0].ID);
            $http({ method: "get", url: "Devices/GetDevice/" + $scope.gridOptions.selectedItems[0].ID }).
            success(function (data, status, headers, config) {
                $scope.DeviceDetail = data;

            });
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        columnDefs: [{ field: 'AgentCompany', displayName: 'Company Name' },
                         { field: 'SerialNumber', displayName: 'Serial No.' },
                         { field: 'CertificateNo', displayName: 'Certificate No.' },
                         { field: 'Status', displayName: 'Status' }],
    };
}

function DeviceDetailsCtrl($http) {
    var scope = this;
    scope.Device = {
        AgentName: "",
        AgentCompany: "",
        SerialNumber: "",
        Status: "",
        CertificateNo: "",
        SoldBy: "",
        DateSold: "",
        DateInstalled: ""
    }

    scope.Load = function (id) {

        if (typeof (id) !== "undefined") {
            $http({ method: "get", url: "/Device/GetDevice/" + 1 }).
            success(function (data, status, headers, config) {
                scope.Device = data;

                //for (var i = 0; i < data.jsonProducts.length; i++) {
                //    var jp = data.jsonProducts[i];
                //    $scope.products.value.push(jp); //new ProductsVM(jp.ID, jp.Name, jp.UnitPrice));
                //}

                //for (var i = 0; i < data.jsonAgents.length; i++) {
                //    var et = data.jsonAgents[i];
                //    $scope.agents.value.push(et);
                //}
                //$scope.product = $scope.products.value[0];
                //$scope.agent = $scope.agents.value[0];
            });
        }
    }
}

function CertificateCtrl($http, $scope, $modal) {

    $scope.Certificate = {
        ID: "",
        CertNo: "",
        SerialNo: "",
        AgentName: "",
        Status: "",
        DateIssued: "",
    }

    $scope.CertAssign = {
        AgentID: "",
        CertStart: "",
        Range: ""
    }

    $scope.CertList = [];
    $scope.agents = { value: [] };
    $scope.agent = {};



    $scope.Search = function setFilterText(value) {
        $scope.filterOptions.filterText = value;  //+ ";";//Type:' + scope.filterCity + ";";
        console.log(value);
    }

    $scope.Reset = function setFilterText() {
        $scope.filterOptions.filterText = "";  //+ ";";//Type:' + scope.filterCity + ";";
        $scope.filterName = '';
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [250, 500, 1000],
        pageSize: 250,
        currentPage: 1
    };

    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        console.log("data.length " + data.length);
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();

                if ($scope.CertList.length == 0) {
                    $http({ method: "get", url: "/Certificates/GetCertificates" })
                        .success(function (data, status, header, config) {
                            $scope.CertList = data;
                            data = $scope.CertList.filter(function (item) {
                                return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                            });

                            console.log("Filtered1: " + data.length + " page " + page + " pagesize " + pageSize
                        );


                            $scope.setPagingData(data, page, pageSize);
                            $scope.GetCert();
                        });
                } else {

                    data = $scope.CertList.filter(function (item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });

                    $scope.setPagingData(data, page, pageSize);
                }


            } else {
                if ($scope.CertList.length == 0) {
                    $http({ method: "get", url: "/Certificates/GetCertificates" })
                        .success(function (data, status, header, config) {
                            $scope.CertList = data;
                            $scope.setPagingData($scope.CertList, page, pageSize);
                            $scope.GetCert();
                        });
                }
                else {
                    $scope.setPagingData($scope.CertList, page, pageSize);
                }


            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        totalServerItems: $scope.totalServerItems,
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        selectedItems: $scope.mySelections,
        columnDefs: [{ field: 'SerialNo', displayName: 'Serial No' },
                     { field: 'AgentName', displayName: 'Agent Company' },
                     { field: 'CertNo', displayName: 'Certificate No' },
                     { field: 'Status', displayName: 'Status' },
                     { field: 'DateIssued', displayName: 'Date Issued' }]
    };

    $scope.AddCertificate = function (range) {
        $http({ method: "get", url: "/Certificates/AddCertificates?range=" + range })
        .success(function (data, status, header, config) {
            if (status >= 200 && status < 300) {
                $scope.CertList = data.Data.Data;
                alert("Certificates successfully added");
                $scope.range = "";
                $scope.GetCert();
            }
            else {
                alert("Something went wrong");
            }
        });
    }

    $scope.items = [];
    $scope.AssignCertificates = function () {
        $scope.CertAssign.AgentID = $scope.agent.ID;
        //   alert("Agent Id: " + $scope.CertAssign.AgentID + " Cert Begin " + $scope.CertAssign.CertStart + " number " + $scope.CertAssign.Range);
        $http({ method: "post", data: $scope.CertAssign, url: "/Certificates/AssignCertificate" }).
        success(function (data, status, headers, config) {
            if (data.Status == true) {
                $scope.CertList = data.Data.Data;
                $scope.myData = data.Data.Data;

            } else {
                $scope.Errors = data.Data.ErrorMessages;
                $scope.items = $scope.Errors;
                $scope.open(10);
            }

            $scope.GetCert();
        });
    }

    $scope.Load = function () {
        $scope.gridOptions.showSelectionCheckbox = false;
        $http({ method: "get", url: "/Sales/getdropdown" }).
        success(function (data, status, headers, config) {

            for (var i = 0; i < data.jsonAgents.length; i++) {
                var et = data.jsonAgents[i];
                $scope.agents.value.push(et);
            }

            $scope.agent = $scope.agents.value[0];
        });
    }

    $scope.GetCert = function () {
        for (var i = 0; i < $scope.CertList.length; i++) {
            if ($scope.CertList[i].Status == "NOTISSUED") {
                $scope.CertAssign.CertStart = $scope.CertList[i].SerialNo;
                $scope.CertAssign.Range = 0;
                break;
            }
        }
        $scope.CertAssign.Range = 0;
    }

    $scope.Load();
    //==================================================================

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

}

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.
function ModalInstanceCtrl($scope, $modalInstance, items) {
    //angular.module('ui.bootstrap.demo').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {
    $scope.Errors = items;
    $scope.ok = function () {
        $modalInstance.dismiss('cancel');
    };

    //$scope.cancel = function () {
    //    $modalInstance.dismiss('cancel');
    //};
}