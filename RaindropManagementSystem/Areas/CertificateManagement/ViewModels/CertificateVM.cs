﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.CertificateManagement.ViewModels
{
    public class CertAssignVM
    {
        public int AgentID { get; set; }
        public string CertStart { get; set; }
        public int Range { get; set; }
   }

    public class CertListVM
    {
        public int ID { get; set; }
        public string CertNo { get; set; }
        public string SerialNo { get; set; }
        public String AgentName { get; set; }
        public String Status { get; set; }
        public String DateIssued { get; set; }
    }

    public class CertDetailVM
    {
        public String AgentName { get; set; }
        public String CertNo { get; set; }
        public String DateIssued { get; set; }
        public String MyProperty { get; set; }
    }
}

/*

 <dl class="dl-horizontal">
                    <dt>
                        @Html.DisplayNameFor(model => model.Agent.AgentID)
                    </dt>

                    <dd>
                        @Html.DisplayFor(model => model.Agent.AgentID)
                    </dd>

                    <dt>
                        @Html.DisplayNameFor(model => model.CertificateNumber)
                    </dt>

                    <dd>
                        @Html.DisplayFor(model => model.CertificateNumber)
                    </dd>

                    <dt>
                        @Html.DisplayNameFor(model => model.DateIssued)
                    </dt>

                    <dd>
                        @Html.DisplayFor(model => model.DateIssued)
                    </dd>

                    <dt>
                        @Html.DisplayNameFor(model => model.Status)
                    </dt>

                    <dd>
                        @Html.DisplayFor(model => model.Status)
                    </dd>

                    <dt>
                        @Html.DisplayNameFor(model => model.Approved)
                    </dt>

                    <dd>
                        @Html.DisplayFor(model => model.Approved)
*/