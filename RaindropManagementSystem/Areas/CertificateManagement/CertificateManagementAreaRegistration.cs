﻿using System.Web.Mvc;

namespace RaindropManagementSystem.Areas.CertificateManagement
{
    public class CertificateManagementAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CertificateManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CertificateManagement_default",
                "CertificateManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}