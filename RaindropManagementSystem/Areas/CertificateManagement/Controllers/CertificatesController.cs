﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;
using RaindropManagementSystem.Areas.CertificateManagement.ViewModels;
using RaindropManagementSystem.Helper;

namespace RaindropManagementSystem.Areas.CertificateManagement.Controllers
{
    [Authorize]
    public class CertificatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        ErrorClass Errors;

        // GET: CertificateManagement/Certificates
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetCertificates()
        {
            List<Certificate> certificates = new List<Certificate>();
            if (User.IsInRole("Agent"))
            {
                String userId = User.Identity.GetUserId();
                certificates = await db.Certificates.Include(x => x.Agent).Where(x => x.Agent.AgentID == userId).ToListAsync();
            
            }
            else
            {
                certificates = await db.Certificates.Include(x => x.Agent).ToListAsync();
            }

            var output = AutoMapper.Mapper.Map<List<CertListVM>>(certificates.OrderBy(x=> x.ID));
            return Json(output, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This methods adds certificats to the systems that will be assigned to agents. It a
        /// </summary>
        /// <param name="range"></param>
        public async Task<ActionResult> AddCertificates(int range)
        {
            ClientData returnValue = new ClientData
            {
                Data = null,
                Status = true
            };
            int capacity = db.Certificates.Count(); //get the certificate count from the db
            Int32 certnumber = 1001000; //Initialize the certificate number if none has been added

            if (capacity != 0) //executes incase the db already has values
            {
                var cert = db.Certificates.ToList().Last(); //gets the last certificate number
                certnumber = cert.GetCertificateSerialNo(); // set the certificate number to the last certificate number
            }

            for (int i = 1; i <= range; i++) // add the certificate to the db
            {
                var cert = new Certificate { Status = CertificateStatus.notissued };
                cert.SetCertificateSerialNo(certnumber + i);
                db.Certificates.Add(cert);
            }

            db.SaveChanges(); //persisting to the database
          

            if (returnValue.Status == true)
            {
                returnValue.Data = await GetCertificates();
            }
            else
            {

            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method is used to Assign certificates to an agent. 
        /// It takes in a CertViewModel and modifies the tables.
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AssignCertificate(CertAssignVM vm)
        {
            ClientData returnValue = new ClientData
            {
                Data = null,
                Status = true
            };

            Errors = new ErrorClass();

            //get certificate using certificate number
            var Cert = db.Certificates.Where(x => x.CertificateSerialNo == vm.CertStart).SingleOrDefault();

            //Check to see if the certifacate was found or not
            if (Cert != null)
            {
                //convert certificate numebr to int for incrementing purposes
                Int32 certNumber = Cert.GetCertificateSerialNo();

                //Get the last certificate in the system
                var lastCert = db.Certificates.ToList().Last().GetCertificateSerialNo();

                if (lastCert >= certNumber + vm.Range - 1)  //check if last certificate is greater than the the last one that will be assigned
                {
                    for (int i = 0; i < vm.Range; i++)
                    {
                        var cert = new Certificate(); // create a new certificate object
                        cert.SetCertificateSerialNo(certNumber + i); //set the certificate number to the assigned one
                        cert = db.Certificates.Where(x => x.CertificateSerialNo == cert.CertificateSerialNo).Single(); //search db for certificate 

                        if (cert.Status == CertificateStatus.notissued) //only assign certificates that have not been issued
                        {
                            cert.AgentID = vm.AgentID; //Assign certificate to agent
                            cert.Status = CertificateStatus.assigned;
                        }
                        else
                        {
                            certNumber = cert.GetCertificateSerialNo(); //get the previous certificate number
                            for (; i > 0; i--)
                            {
                                cert = new Certificate(); // create a new certificate object
                                cert.SetCertificateSerialNo(certNumber - i); //set the certificate number to the assigned one
                                cert = db.Certificates.Where(x => x.CertificateSerialNo == cert.CertificateSerialNo).Single(); //search db for certificate
                                cert.AgentID = null; //assign agent to null
                                cert.Status = CertificateStatus.notissued; //change back the status to not issued
                            }

                            //Create error message for user to prevent overriding already issued certificates
                            Errors.ErrorMessages.Add("Cannont assign certifacates within this range: " + Cert.CertificateSerialNo + " to HD" + (Cert.GetCertificateSerialNo() + vm.Range - 1));
                            Errors.ErrorMessages.Add("Certificate Number HD" + certNumber + " has already been assigned");
                            returnValue.Status = false;
                            break;
                        }
                        db.Entry(cert).State = EntityState.Modified; // add modify flag to show edit was made
                    }

                 await db.SaveChangesAsync(); //persist changes when all is done
                }
                else
                {
                    //return message saying that the assignment will be more that the certificates in the system
                    Errors.ErrorMessages.Add("Please add more certificates to the system. Some certifcate will fall or of range");
                    returnValue.Status = false;
                }
            }
            else
            {
                //TODO: add error message saying that the certificate has not be found please check the certificate number again
                Errors.ErrorMessages.Add(vm.CertStart.ToUpper() + " is an invalid certificate number please check the certificate number again. ");
                Errors.ErrorMessages.Add("Required format is HD 1XXXXXX e.g HD1001000, HD10010050 ");
                returnValue.Status = false;
            }

            //populate the return value with the right Content to return
            if (returnValue.Status == true)
            {
                returnValue.Data = await GetCertificates();
            }
            else
            {
                returnValue.Data = Errors;
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        // GET: CertificateManagement/Certificates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Certificate certificate = await db.Certificates.FindAsync(id);
            if (certificate == null)
            {
                return HttpNotFound();
            }
            return View(certificate);
        }

        // GET: CertificateManagement/Certificates/Create
        public ActionResult Create()
        {
            ViewBag.AgentID = new SelectList(db.AgentDetails, "ID", "AgentID");
            return View();
        }

        // POST: CertificateManagement/Certificates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,CertificateNumber,DateIssued,Status,AgentID,Approved")] Certificate certificate)
        {
            if (ModelState.IsValid)
            {
                db.Certificates.Add(certificate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AgentID = new SelectList(db.AgentDetails, "ID", "AgentID", certificate.AgentID);
            return View(certificate);
        }

        // GET: CertificateManagement/Certificates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Certificate certificate = await db.Certificates.FindAsync(id);
            if (certificate == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgentID = new SelectList(db.AgentDetails, "ID", "AgentID", certificate.AgentID);
            return View(certificate);
        }

        // POST: CertificateManagement/Certificates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,CertificateNumber,DateIssued,Status,AgentID,Approved")] Certificate certificate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(certificate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AgentID = new SelectList(db.AgentDetails, "ID", "AgentID", certificate.AgentID);
            return View(certificate);
        }

        // GET: CertificateManagement/Certificates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Certificate certificate = await db.Certificates.FindAsync(id);
            if (certificate == null)
            {
                return HttpNotFound();
            }
            return View(certificate);
        }

        // POST: CertificateManagement/Certificates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Certificate certificate = await db.Certificates.FindAsync(id);
            db.Certificates.Remove(certificate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
