﻿using System.Web.Mvc;

namespace RaindropManagementSystem.Areas.ExpenditureManagement
{
    public class ExpenditureManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExpenditureManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExpenditureManagement_default",
                "ExpenditureManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                name: "ExpenditureOverride",
                url: "ExpenditureManagement/{*.}",
                defaults: new { controller = "Expenditure", action = "Index" }
            );

        }
    }
}