﻿using Dal;
using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels
{
    public class ExpenditureViewModel
    {
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public double Amount { get; set; }
        [Required]
        [Display(Name = "Float Account")]
        public int FloatAccountID { get; set; }
        [Required]
        [Display(Name = "Exp Type")]
        public int ExpenditureTypeID { get; set; }
    }

    public class ExpenditureTransactionVM
    {
        public int ID { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Display(Name = "Status")]
        public ExpenditureStatus Status { get; set; }

        [Display(Name = "Type")]
        public ExpenditureType ExpenditureType { get; set; }

        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }

        [Display(Name = "Date Finalized")]
        public DateTime? DateApproved { get; set; }

        [Display(Name = "Initiated By")]
        public string InitiatedBy { get; set; }

        [Display(Name = "Finalized By")]
        public string ApprovedBy { get; set; }
    }

    public class ExpenditureDetailVM : ExpenditureTransactionVM
    {
        [Display(Name = "Comments")]
        public string Comment { get; set; }
    }


    public class JsonExpenditure
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string DateInitiated { get; set; }
        public string UserId { get; set; }
        public string DateApproved { get; set; }

        public string InitiatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string Comment { get; set; }

        public JsonExpenditure Create(ExpenditureTransaction et)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            this.ID = et.ID;
            this.Description = et.Description;
            this.Amount = et.Amount;
            this.Status = et.Status.ToString();
            this.Type = et.ExpenditureType.Name;
            this.DateInitiated = String.Format("{0: d/M/yyyy HH:mm:ss}", et.DateInitiated);
            var user = db.Users.ToList().Where(x => x.Id == et.InitiatedBy).SingleOrDefault();
            this.InitiatedBy = user.FullNames;
            this.UserId = et.InitiatedBy;

            user = db.Users.ToList().Where(x => x.Id == et.ApprovedBy).SingleOrDefault();
            if (user != null)
            {
                this.DateApproved = String.Format("{0: dd/MM/yyyy hh:mm}", et.DateApproved);
                this.ApprovedBy = user.FullNames;
            }

            this.Comment = et.Comment;
            return this;
        }
    }
}