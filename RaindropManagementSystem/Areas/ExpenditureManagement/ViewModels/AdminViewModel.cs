﻿using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels
{
    public class AdminViewModel
    {
    }

    public class AdminFloatStatus
    {
        public bool Status { get; set; }
        public double Amount { get; set; }
    }

    

    public class AdminApproveViewModel
    {

        public int ID { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Amount")]
        public double Amount { get; set; }
        [Display(Name = "Float Account")]
        public string AccountName { get; set; }
        [Display(Name = "Status")]
        public ExpenditureStatus Status { get; set; }
        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }
        [Display(Name = "Date Approved")]
        public DateTime? DateApproved { get; set; }
        [Display(Name = "Initiated By")]
        public string UserNames { get; set; }
        [Display(Name = "Expenditure Type")]

        public int ExpenditureTypeID { get; set; }
        [Display(Name = "Comment")]
        [Required]
        public string Comments { get; set; }
    }
}