﻿function FloatAccount(ID, Amount, Name) {
    this.ID = ID;
    this.Amount = Amount;
    this.Name = Name;
}
function ExpenditureType(ID, Name) {
    this.ID = ID;
    this.Name = Name;
}


function IndexCtrl($scope, $http, LoadData) {
    $scope.Transactions =
    {
        "ID": "",
        "Description": "",
        "Amount": "",
        "Status": "",
        "Type": "",
        "DataInitiated": "",
        "DateApproved": "",
        "InitiatedBy": "",
        "ApprovedBy": "",
        "Comment": "",
        "UserId": ""
    }

    $scope.User = {
        "AccountType": "",
        "UserId": "",
    }
   


    $scope.ShowCreate = function (acountType) {
        if (accountType == "SuperAdmin") {
            return false;
        }
        else
            return true;
    }

    //LoadData.async().then(function () {
    //    var data = LoadData.data();
    //    $scope.User.AccountType = data.AccountType;
    //    $scope.User.UserId = data.UserId;
    //    $scope.Transactions = data.Transactions;
    //});


    $scope.Transactions = [];

    $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };


    $scope.Search = function setFilterText(value) {
        $scope.filterOptions.filterText = value;  //+ ";";//Type:' + $scope.filterCity + ";";
    }

    $scope.Reset = function setFilterText() {
        $scope.filterOptions.filterText = "";  //+ ";";//Type:' + $scope.filterCity + ";";
        $scope.filterName = '';
    }

    $scope.mySelections = [];
    $scope.Transactions = [];
    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                LoadData.async().then(function () {
                    data = LoadData.data();
                    $scope.User.AccountType = data.AccountType;
                    $scope.User.UserId = data.UserId;
                    $scope.Transactions = data.Transactions.filter(function (item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;

                    });

                    $scope.setPagingData($scope.Transactions, page, pageSize);
                });
            } else {

                LoadData.async().then(function () {
                    data = LoadData.data();
                    $scope.User.AccountType = data.AccountType;
                    $scope.User.UserId = data.UserId;
                    $scope.Transactions = data.Transactions;

                    $scope.setPagingData($scope.Transactions, page, pageSize);
                });
            }
        }, 100);
    };
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        columnDefs: [{ field: 'Description', displayName: 'Description' },
                         { field: 'Amount', displayName: 'Amount' },
                         { field: 'Status', displayName: 'Status' },
                         { field: 'Type', displayName: 'Type' },
                         { field: 'InitiatedBy', displayName: 'Initiated By' },
                         { field: 'DateInitiated', displayName: 'Date Initiated' },
                         { field: 'ApprovedBy', displayName: 'Approved By' },
                         { field: 'DateApproved', displayName: 'Date Approved' }],
    };


    $scope.HideDetails = function () {
        $scope.mySelections[0].__ng_selected__ = false;
    }

    $scope.ApproveTransaction = function (id, type) {
        $scope.approveTransaction =
            {
                "ID": id,
                "Comments": $scope.Transaction.Comments,
                "Status": type
            }

        alert("ID " + $scope.approveTransaction.ID + " comments " + $scope.approveTransaction.Comments + " Status " + $scope.approveTransaction.Status);
        //make a call to server to add data
        $http({ method: "POST", data: $scope.approveTransaction, url: "Expenditure/Approve" }).
            success(function (data, status, headers, config) {
                if (data.Status) {
                    // $scope.Transactions = data.Data;
                   // $scope.myData = data.Data.Transactions;
                    $scope.mySelections[0].__ng_selected__ = false;

                    alert("The transaction has been " + $scope.approveTransaction.Status);
                    $scope.approveTransaction = {};
                    $scope.Transaction.Comments = "";
                }
                else {
                    alert("Status " + data.Data.ErrorMessages[0]);
                }
            });
    }
}

function RaiseRequestCtrl($scope, $http, LoadData) {
    $scope.floatAccounts = { value: [] };
    $scope.expenditureTypes = { value: [] };
    $scope.Errors = [];
    $scope.showError = { show: false };
    $scope.Description = "";
    $scope.Amount = "";
    $scope.raiseRequest = {
        "Description": "",
        "Amount": "",
        "FloatAccountID": "",
        "ExpenditureTypeID": ""
    }

    $scope.floatAccount = {};
    $scope.expenditureType = {};

    $scope.PopulateRequest = function () {
        $scope.showError.show = false;
        $http({ method: "GET", url: "Expenditure/GetDropDownMenu" }).
             success(function (data, status, headers, config) {

                 if (data.floatAccounts.length == 0) {
                     alert("There are no float accounts");
                 } else {
                     $scope.flaotAccount = {};
                     $scope.expenditureType = {};

                     for (var i = 0; i < data.floatAccounts.length; i++) {
                         var fa = data.floatAccounts[i];
                         $scope.floatAccounts.value.push(new FloatAccount(fa.ID, fa.Amount, fa.AccountName));
                     }

                     for (var i = 0; i < data.expenditureTypes.length; i++) {
                         var et = data.expenditureTypes[i];
                         $scope.expenditureTypes.value.push(new ExpenditureType(et.ID, et.Name));
                     }

                     $scope.floatAccount = $scope.floatAccounts.value[0];
                     $scope.expenditureType = $scope.expenditureTypes.value[0];
                 }
             });
    }

    $scope.RaiseRequest = function () {
        $scope.raiseRequest = {
            "Description": $scope.Description,
            "Amount": $scope.Amount,
            "FloatAccountID": $scope.floatAccount.ID,
            "ExpenditureTypeID": $scope.expenditureType.ID
        }


        $http({ method: "Post", data: $scope.raiseRequest, url: "Expenditure/RaiseRequest" }).
      success(function (data, status, headers, config) {
          if (data.Status) {
              $scope.Description = "";
              $scope.Amount = "";
              alert("Your transfer request has be processed");
          }
          else {
              $scope.showError.show = true;
              $scope.Errors = data.Data.ErrorMessages;
              $scope.Description = "";
              $scope.Amount = "";
          }
      });
    }


    $scope.PopulateRequest();
}