﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using RaindropManagementSystem.Helper;
using Dal;
using Models;
using RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using RaindropManagementSystem.Generics;


namespace RaindropManagementSystem.Areas.ExpenditureManagement.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class ExpenditureController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: ExpenditureAdmin
        public ActionResult Index(bool status = false)
        {
            if (status == true)
            {
                ViewBag.ErrorMessage = "No Float Account Available";
            }

            return View();

        }

        public async Task<ActionResult> Expenditures()
        {
            var data = await GetExpenditures();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<Object> GetExpenditures()
        {
            List<ExpenditureTransaction> transactions = new List<ExpenditureTransaction>();
            List<JsonExpenditure> dataReturned = new List<JsonExpenditure>();
            var UserId = User.Identity.GetUserId();
            string roleName = "SuperAdmin";

            if (!User.IsInRole("SuperAdmin"))
            {
                roleName = "Admin";
                transactions = await db.ExpenditureTransactions.Where(x => x.InitiatedBy == UserId || x.ApprovedBy == UserId || x.Status == ExpenditureStatus.Pending).Include(x => x.ExpenditureType).Include(x => x.ExpenditureType).ToListAsync<ExpenditureTransaction>();
            }
            else
            {
                transactions = await db.ExpenditureTransactions.Include(x => x.ExpenditureType).ToListAsync<ExpenditureTransaction>();
            }

            foreach (var item in transactions)
            {
                dataReturned.Add(new JsonExpenditure().Create(item));
            }

            return new TransactionResult<JsonExpenditure>(UserId, dataReturned, roleName);
        }

        /// <summary>
        /// gets the drop down list for the expenditure request
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> GetDropDownMenu()
        {
            var UserId = User.Identity.GetUserId();
            var floatAccounts = await (from f in db.FloatAccounts
                                       where (f.UserID == UserId && f.FloatAccountType != FloatAccountType.Primary)
                                       select new FloatAccountRequest
                                       {
                                           AccountName = f.Name,
                                           Amount = f.Amount,
                                           ID = f.ID
                                       }).ToListAsync<FloatAccountRequest>();

            var expenditureTypes = await (from e in db.ExpenditureTypes
                                          select new { e.ID, e.Name }).ToListAsync();

            return Json(new { floatAccounts, expenditureTypes }, JsonRequestBehavior.AllowGet);
        }

        //GET: Admin
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();
            var floatAccount = db.FloatAccounts.Where(x => x.UserID == userId && x.Status == true);

            ViewBag.FloatAccountID = new SelectList(floatAccount, "ID", "Name");
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name");
            return PartialView("_Create");

        }

        [HttpPost]
        public async Task<ActionResult> RaiseRequest([Bind(Include = "Description,Amount,FloatAccountID,ExpenditureTypeID")]ExpenditureViewModel evm)
        {
            ClientData result = new ClientData
            {
                Status = false,
                Data = null
            };

            string userId = User.Identity.GetUserId();
            var floatAccount = db.FloatAccounts.Where(x => x.UserID == userId && x.Status == true);
            int count = floatAccount.Count();

            if (ModelState.IsValid)
            {
                var afsvm = AdminDB.CheckFloatAccount(evm.FloatAccountID);

                if (afsvm.Status == false)
                {
                    ModelState.AddModelError("", "Float Account Not active");
                }
                else
                {
                    if (afsvm.Amount < evm.Amount)
                    {
                        ModelState.AddModelError("", "Insufficient Funds to perform this transaction");
                    }
                    else
                    {
                        try
                        {
                            ExpenditureTransaction expenditureTransactions = new ExpenditureTransaction()
                            {
                                Amount = evm.Amount,
                                Description = evm.Description,
                                ExpenditureTypeID = evm.ExpenditureTypeID,
                                FloatAccountID = evm.FloatAccountID
                            };

                            expenditureTransactions.DateInitiated = HelperClass.DateTimeNow();
                            expenditureTransactions.InitiatedBy = userId;
                            expenditureTransactions.Status = ExpenditureStatus.Pending;

                            db.ExpenditureTransactions.Add(expenditureTransactions);
                            db.SaveChanges();
                            ExpenditureDB.RaiseRequest(expenditureTransactions); //Removes funds from a particular float account
                            result.Status = true;
                            result.Data = await GetExpenditures();
                        }
                        catch
                        {
                            ModelState.AddModelError("", "Something went wrong re-try the transactions");
                        }
                    }
                }
            }

            if (result.Status == false)
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Approve([Bind(Include = "ID,Comments,Status")]AdminApproveViewModel aavm)
        {

            ClientData result = new ClientData
            {
                Status = false,
                Data = null
            };

            if (ModelState.IsValid)
            {
                var expenditureTransaction = await db.ExpenditureTransactions.FindAsync(aavm.ID);

                switch (aavm.Status)
                {
                    case ExpenditureStatus.Approved:
                        {
                            var userId = User.Identity.GetUserId();
                            expenditureTransaction.ApprovedBy = userId;
                            expenditureTransaction.Status = ExpenditureStatus.Approved;
                            expenditureTransaction.DateApproved = HelperClass.DateTimeNow();

                            db.Entry(expenditureTransaction).State = EntityState.Modified;
                            await db.SaveChangesAsync();

                            ExpenditureDB.ApproveRequest(expenditureTransaction);

                            result.Status = true;
                            result.Data = await GetExpenditures();
                            break;
                        }
                    case ExpenditureStatus.Declined:
                        {
                            var userId = User.Identity.GetUserId();
                            expenditureTransaction.ApprovedBy = userId;
                            expenditureTransaction.Status = ExpenditureStatus.Declined;
                            expenditureTransaction.DateApproved = HelperClass.DateTimeNow();

                            db.Entry(expenditureTransaction).State = EntityState.Modified;
                            await db.SaveChangesAsync();

                            ExpenditureDB.DeclineRequest(expenditureTransaction);

                            result.Status = true;
                            result.Data = await GetExpenditures();
                            break;
                        }
                    case ExpenditureStatus.Pending:
                        break;

                    default:
                        break;
                }
            }

            if (result.Status == false)
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
