﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.SalesManagement.ViewModels
{
    public class SalesCreateVM
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double BuyingPrice { get; set; }
        public double Total { get; set; }
        public int AgentID { get; set; }
        public string AgentName { get; set; }

        public String[] Devices { get; set; }
    }

    public class AgentSaleVM
    {
        public int ID { get; set; }
        public string CompanyName { get; set; }
    }

    public class SalesList
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double  BuyingPrice { get; set; }
        public double Total { get; set; }
        public string CompanyName { get; set; }
        public string SoldBy { get; set; }
        public string Timestamp { get; set; }
    }

    public class DeviceAssingment
    {
        public int saleID { get; set; }
        public string[] SerialNumbers { get; set; }
    }
}