﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Areas.SalesManagement.ViewModels;
using RaindropManagementSystem.Helper;
using Microsoft.AspNet.Identity;
namespace RaindropManagementSystem.Areas.SalesManagement.Controllers
{
    [Authorize]
    public class SalesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SalesManagement/Sales
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Transactions()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ProcessSale([Bind(Include = "ProductID,Quantity,BuyingPrice,AgentID,Devices")] SalesCreateVM salesVM)
        {
            if (ModelState.IsValid)
            {
                //creating a sales object from the view model
                var sales = AutoMapper.Mapper.Map<Sales>(salesVM);
                sales.Timestamp = HelperClass.DateTimeNow();
                sales.SalesAgentID = User.Identity.GetUserId();

                db.Sales.Add(sales);

                //reducing the units in stock with the number of items sold
                var product = await db.Products.FindAsync(sales.ProductID);
                product.UnitsInStock -= sales.Quantity;
                db.Entry(product).State = EntityState.Modified;

                //updating the database
                await db.SaveChangesAsync();

                //Adding devices after sale has been perfomred
                foreach (var serial in salesVM.Devices)
                {
                    Device device = new Device { SalesID = sales.ID, SerialNumber = serial, status = DeviceStatus.issued };
                    db.Devices.Add(device);
                }

                await db.SaveChangesAsync();


                //returning the sales after sales is performed
                var saleItem = await db.Sales.Where(x => x.ID == sales.ID).Include(x => x.Product).Include(x => x.AgentDetail).Include(x => x.SalesAgent).SingleOrDefaultAsync();

                var jsonReturn = AutoMapper.Mapper.Map<SalesList>(saleItem);

                return Json(jsonReturn, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public async Task<ActionResult> AssignDevices(DeviceAssingment deviceAssignment)
        {
            int saleID = deviceAssignment.saleID;

            if (ModelState.IsValid)
            {
                foreach (var serial in deviceAssignment.SerialNumbers)
                {
                    Device device = new Device { SalesID = saleID, SerialNumber = serial, status = DeviceStatus.issued };
                    db.Devices.Add(device);
                }

                await db.SaveChangesAsync();
                return Json(await db.Devices.ToListAsync(), JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        public async Task<ActionResult> GetTransactions()
        {
            var salesList = await db.Sales.Include(x => x.Product).Include(x => x.AgentDetail).ToListAsync();
            var jsonReturn = AutoMapper.Mapper.Map<List<SalesList>>(salesList);

            return Json(jsonReturn, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetDevices()
        {
            return Json(await db.Devices.ToListAsync(), JsonRequestBehavior.AllowGet);
        }


        // GET: SalesManagement/Sales/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales sales = await db.Sales.FindAsync(id);
            if (sales == null)
            {
                return HttpNotFound();
            }
            return View(sales);
        }

        // POST: SalesManagement/Sales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,ProductID,Quantity,BuyingPrice,AgentDetailID,SalesAgentID,Timestamp")] Sales sales)
        {
            if (ModelState.IsValid)
            {
                db.Sales.Add(sales);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AgentDetailID = new SelectList(db.AgentDetails, "ID", "AgentID", sales.AgentDetailID);
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", sales.ProductID);
            return View(sales);
        }

        // GET: SalesManagement/Sales/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales sales = await db.Sales.FindAsync(id);
            if (sales == null)
            {
                return HttpNotFound();
            }

            ViewBag.AgentDetailID = new SelectList(db.AgentDetails, "ID", "AgentID", sales.AgentDetailID);
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", sales.ProductID);

            return View(sales);
        }

        // POST: SalesManagement/Sales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ProductID,Quantity,BuyingPrice,AgentDetailID,SalesAgentID,Timestamp")] Sales sales)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sales).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AgentDetailID = new SelectList(db.AgentDetails, "ID", "AgentID", sales.AgentDetailID);
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", sales.ProductID);
            return View(sales);
        }

        public async Task<ActionResult> GetDropDown()
        {
            var products = await db.Products.ToListAsync();
            var jsonProducts = AutoMapper.Mapper.Map<List<JsonProduct>>(products);

            var agents = await db.AgentDetails.ToListAsync();
            var jsonAgents = AutoMapper.Mapper.Map<List<AgentSaleVM>>(agents);
            return Json(new { jsonProducts, jsonAgents }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
