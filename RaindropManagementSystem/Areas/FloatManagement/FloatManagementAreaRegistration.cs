﻿using System.Web.Mvc;

namespace RaindropManagementSystem.Areas.FloatManagement
{
    public class FloatManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FloatManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FloatManagement_default",
                "FloatManagement/{controller}/{action}/{id}",
                defaults: new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "RaindropManagementSystem.Areas.FloatManagement.Controllers" }
            );

            context.MapRoute(
       name: "FloatOverride",
       url: "FloatManagement/{*.}",
       defaults: new { controller = "Account", action = "Index" }
   );
        }
    }
}