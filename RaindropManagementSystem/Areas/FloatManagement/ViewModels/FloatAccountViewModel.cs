﻿using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.FloatManagement.ViewModels
{
    public class FloatAccountViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

    }
    public class FloatAccountEditViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Status")]
        public bool Status { get; set; }
    }

    public class FloatAccountDetailViewModel : FloatAccountViewModel
    {

        public int ID { get; set; }

        [Required]
        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Required]
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Required]
        [Display(Name = "Date Modified")]
        public DateTime DateModified { get; set; }
    }

    public class FloatAccountTransfer
    {
        public int FloatAccountID { get; set; }

        [Display(Name = "Account From")]
        public string FloatAccountName { get; set; }
        [Required]
        [Display(Name = "To")]
        public int FundsTo { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Required]
        [Display(Name = "Comment")]
        public string Description { get; set; }
    }

    public class FloatAccountAdd
    {
        public int ID { get; set; }

        [Display(Name = "Account")]
        public string AccountName { get; set; }

        [Required]
        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Required]
        [Display(Name = "Comment")]
        public string Description { get; set; }
    }

    public class FloatAccountRequest
    {
        public int ID { get; set; }

        [Display(Name = "Account")]
        public string AccountName { get; set; }

        [Required]
        [Display(Name = "Amount")]
        public double Amount { get; set; }

    }

    public class FloatAccountAssignVM
    {
        public int ID { get; set; }
        public string AccountName { get; set; }
        public string UserId { get; set; }
    }


    /// <summary>
    /// Used to return the transaction number after modificationat of the float account incase a reversa is required
    /// </summary>
    public class FloatAccountResult
    {
        public bool Status { get; set; }
        public int ID { get; set; }
    }
}