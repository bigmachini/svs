﻿using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.FloatManagement.ViewModels
{
    public class FloatAdminDetailVM
    {
        public int ID { get; set; }
        [Display(Name = "Account Name")]
        public string Name { get; set; }
        [Display(Name = "Actual Balance")]
        public double Amount { get; set; }
        [Display(Name = "Book Balance")]
        public double AmountHeld { get; set; }
        [Display(Name = "Linked To")]
        public string UserNames { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Acount Status")]
        public bool Status { get; set; }
        [Display(Name = "Account Type")]
        public FloatAccountType FloatAccountType { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Date Modified")]
        public DateTime DateModified { get; set; }
    }

    public class FloatAdminRequestVM
    {
        public int ID { get; set; }
        [Display(Name = "Initiated by")]
        public string InitiatedBy { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Amount")]
        public double Amount { get; set; }
        [Display(Name = "Status")]
        public EnumStatus Status { get; set; }

        [Display(Name = "Type")]
        public EnumTransactionType Type { get; set; }
     
        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }
    }


}