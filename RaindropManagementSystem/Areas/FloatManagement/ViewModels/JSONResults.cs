﻿using Dal;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.FloatManagement.ViewModels
{
    public class JsonAccount
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public double AmountHeld { get; set; }
        public string UserNames { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string DateCreated { get; set; }
        public string DateModified { get; set; }

        public JsonAccount Create(FloatAccount favm)
        {
            this.ID = favm.ID;
            this.Name = favm.Name;
            this.Amount = favm.Amount;
            this.AmountHeld = favm.AmountHeld;
            this.DateCreated = String.Format("{0: d/M/yyyy HH:mm:ss}", favm.DateCreated);
            this.DateModified = String.Format("{0: d/M/yyyy HH:mm:ss}", favm.DateModified);
            this.Description = favm.Description;
            this.Status = (favm.Status) ? "Active" : "Inactive";
            this.Type = favm.FloatAccountType.ToString();
            return this;
        }

        public string GetUserName(FloatAccount favm)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var user = db.Users.ToList().Where(x => x.Id == favm.UserID).SingleOrDefault();
            string userName = "";
            if (user != null)
            {
                userName = user.FullNames;
            }

            return userName;
        }
    }

    public class JsonTransaction
    {
        public int ID { get; set; }
        public string Description { get; set; }

        public double Amount { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string DateInitiated { get; set; }

        public string DateApproved { get; set; }

        public string InitiatedBy { get; set; }

        public string ApprovedBy { get; set; }

        public string Comments { get; set; }
        public string UserId { get; set; }

        public JsonTransaction Create(FloatTransaction ft)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            this.ID = ft.ID;
            this.Amount = ft.Amount;
            this.Comments = ft.Comments;
            this.DateInitiated = String.Format("{0: dd/MM/yyyy hh:mm}", ft.DateAuthorized);
            this.UserId = ft.InitiatedBy;
            var user = db.Users.ToList().Where(x => x.Id == ft.InitiatedBy).SingleOrDefault();
            this.InitiatedBy = user.FullNames;

            user = db.Users.ToList().Where(x => x.Id == ft.ApprovedBy).SingleOrDefault();
            if (user != null)
            {
                this.DateApproved = String.Format("{0: dd/MM/yyyy hh:mm}", ft.DateApproved);
                this.ApprovedBy = user.FullNames;
            }

            this.Description = ft.Description;
            this.Status = ft.Status.ToString();
            this.Type = ft.Type.ToString();
            return this;
        }
    }
}