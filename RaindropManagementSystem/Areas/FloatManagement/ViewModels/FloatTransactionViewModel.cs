﻿using Models;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace RaindropManagementSystem.Areas.FloatManagement.ViewModels
{
    public class FloatTransactionViewModel
    {
    }

    public class RequestFundViewModel
    {
        public int ID { get; set; }
        
        [Display(Name = "Amount")]
        [Required]
        public double Amount { get; set; }

        [Display(Name = "Comment")]
        [Required]
        public string Description { get; set; }
    }

    public class ApproveTransaction
    {
        public int ID { get; set; }
        [Required]
        public string Comments { get; set; }
        public string Status { get; set; }
    }

    public class ApproveFloatTransactionVM
    {
        public int ID { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Amount")]
        public double Amount { get; set; }

        [Display(Name = "Commnents")]
        [Required]
        public string Comments { get; set; }
        [Display(Name = "Status")]
        public EnumStatus Status { get; set; }
        [Display(Name = "Transaction Type")]
        public EnumTransactionType Type { get; set; }
        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }
        [Display(Name = "Initiated By")]
        public string InitiatedBy { get; set; }
    }

    public class FloatTransactionVM
    {
        public int ID { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Amount")]
        public double Amount { get; set; }
                
        [Display(Name = "Status")]
        public EnumStatus Status { get; set; }
      
        [Display(Name = "Transaction Type")]
        public EnumTransactionType Type { get; set; }
      
        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }

        [Display(Name = "Date Finalized")]
        public DateTime ? DateApproved { get; set; }
    
        [Display(Name = "Initiated By")]
        public string InitiatedBy { get; set; }

        [Display(Name = "Finalized By")]
        public string ApprovedBy { get; set; }
    }

    public class FloatTransactionDetailVM : FloatTransactionVM
    {
        [Display(Name = "Comments")]
        public string Comments { get; set; }
    }


}