﻿function TransferDropDown(ID, Amount, Name) {
    this.ID = ID;
    this.Amount = Amount;
    this.Name = Name;
}

function FloatAccount($scope, $http) {
    //Variables for displaying UI Elements
    $scope.showCreate = { value: false };
    $scope.showDetails = { value: false };
    $scope.showAddFunds = { value: false };
    $scope.showTransfer = { value: false };
    $scope.showRequest = { value: false };
    $scope.showTransaction = { value: false };
    $scope.errors = {};

    $scope.AccountBalance = { "balance": 0 };

    $scope.createAccount = {
        "Name": "",
        "Description": ""
    }

    $scope.addFund = {
        "Name": "",
        "Amount": "",
        "ID":"",
        "Description": ""
    }


    //this variable holds the Account information
    $scope.Account =
        {
            "ID": "",
            "Name": "",
            "Amount": "",
            "AmountHeld": "",
            "UserNames": "",
            "Description": "",
            "Status": "",
            "Type": "",
            "DateCreated": "",
            "DateModified": " ",
            "Index": "",
        }



    $scope.Transaction =
   {
       "ID": "1",
       "Description": "Test",
       "Amount": "",
       "Status": "",
       "Type": "",
       "DateInitiated": "",
       "DateApproved": "",
       "InitiatedBy": "Elisha Waudo",
       "ApprovedBy": "Wambiji Waudo",
       "Comments": ""
   }

    $scope.Transactions = {};
    $scope.gridOptions = {
        data: 'Transactions',
        multiSelect: false,
        columnDefs: [{ field: 'Description', displayName: 'Description' },
                         { field: 'Amount', displayName: 'Amount' },
                         { field: 'Status', displayName: 'Status' },
                         { field: 'Type', displayName: 'Type' },
                         { field: 'InitiatedBy', displayName: 'Initiated By' },
                         { field: 'DateInitiated', displayName: 'Date Initiated' },
                         { field: 'ApprovedBy', displayName: 'Approved By' },
                         { field: 'DateApproved', displayName: 'Date Approved' }],
    };

    //This variable holds the accounts list when gotten from the db
    $scope.Accounts = {};
    $scope.SelectList = { value: [] };

    //This method populates the Accounts with the data from the database
    $scope.$watch("Accounts", function () {

        for (var x = 0; x < $scope.Accounts.length; x++) {
            var account = $scope.Accounts[x];
            account.Index = x;
        }

    })

    //This function is the one that pulls the information from the database
    $scope.Load = function () {
        $http({ method: "GET", url: "Account/GetAccounts" }).
    success(function (data, status, headers, config) {
        $scope.AccountType = data.AccountType;
        $scope.Accounts = data.Accounts;
        $scope.ShowCreate();
    });
    }

    //This method is used to post a new account to the DB
    $scope.CreateAccount = function () {
        $http({
            method: "POST",
            data: $scope.createAccount,
            url: "Account/Create"
        }).success(function (data, status, headers, config) {

            if (data.Status) {
                $scope.Load();
                $scope.HideDiv();
                $scope.ShowCreate();
                alert($scope.createAccount.Name + " has been created successfully");
                $scope.createAccount = {};
               
            }
            else {
                $scope.Errors = data.Data.ErrorMessages;
            }
        });

    }

    $scope.GetAccount = function (id, index) {
        $http({ method: "GET", url: "Account/GetAccount/" + id }).
                success(function (data, status, headers, config) {
                    $scope.Account = data;
                    $scope.Account.Index = index;
                });
    }

    //this method is used to get the details of a particular transaction
    $scope.GetDetails = function (id, index) {
       
        $scope.GetAccount(id, index);
        $scope.HideDiv();
        $scope.showDetails.value = true;
    }

    //This method is used to show or hide the create div depending on the role of the user
    $scope.ShowCreate = function () {

        if ($scope.AccountType == "SuperAdmin") {
            $scope.showCreate.value = true;
        }

        else
            $scope.showCreate.value = false;
    }

    //This clears the model so that its not binding to any data
    $scope.OnHide = function () {
        $scope.Account =
                        {
                            "ID": 0,
                            "Name": "",
                            "Amount": 0,
                            "AmountHeld": 0,
                            "UserNames": "",
                            "Description": "",
                            "Status": "",
                            "Type": "",
                            "DateCreated": "",
                            "DateModified": " "
                        };

        $scope.createAccount = {
            "Name": "",
            "Description": ""
        }


        $scope.HideDiv();
        $scope.ShowCreate();
    }

    $scope.GetTransactions = function (id, index) {
        $scope.Account.Name = $scope.Accounts[index].Name;
        $http({
            method: "GET",
            url: "Account/GetTransactions/" + id
        }).

          success(function (data, status, headers, config) {
              $scope.Transactions = data;
              $scope.showTransaction.value = true;
          });
    }

    //This method populated the object with the right details 
    $scope.PopulateAddFunds = function (index) {
        $scope.HideDiv();
        $scope.addFund.Name = $scope.Accounts[index].Name;
        $scope.addFund.ID = $scope.Accounts[index].ID;
        $scope.showAddFunds.value = true;
    }

    $scope.AddFunds = function () {

        $http({
            method: "POST",
            data: $scope.addFund,
            url: "Account/Add"
        }).success(function (data, status, headers, config) {
            $scope.AccountType = data.AccountType;
            $scope.Accounts = data.Accounts;
            alert($scope.addFund.Amount + " add to " + $scope.addFund.Name + " successfully");
            $scope.HideDiv();
            $scope.ShowCreate();
           

        });
    }

    $scope.CheckTransferAccount = function (amount, amountHeld) {

        var result = amount;
        if (amountHeld < 0) {
            result += amountHeld

        }

        if (result <= 0) {
            result = 0;
        }
        return result;

    }
    //This method populated the object with the right details 
    $scope.PopulateTransfer = function (id, index) {
        $http({ method: "GET", url: "Account/GetAccount/" + id }).
              success(function (data, status, headers, config) {
                  $scope.Account = data;
                  $scope.AccountBalance.balance = $scope.CheckTransferAccount($scope.Account.Amount, $scope.Account.AmountHeld);
                  if ($scope.AccountBalance.balance == 0) {
                      $scope.HideDiv();
                      alert($scope.Account.Name + " Doesn't have sufficient funds for a transfer");
                      $scope.Account = {};
                      $scope.ShowCreate();
                  }
                  else {
                      $scope.TransferName = $scope.Account.Name;
                      $scope.TransferDD = {};
                      $scope.AccountID = $scope.Account.ID;

                      if ($scope.Account.Length == 0) {
                          alert("Transaction is not possible. No account to transfer money to");
                          $scope.showTransfer.value = false;
                          $scope.Account = {};
                          $scope.ShowCreate();
                      } else {

                          $scope.SelectList.value = [];
                          for (var i = 0; i < $scope.Accounts.length; i++) {
                              if (i != index && $scope.Accounts[i].Type != 'Primary' && $scope.Accounts[i].Status == 'Active') {
                                  var account = $scope.Accounts[i];
                                  $scope.SelectList.value.push(new TransferDropDown(account.ID, account.Amount, account.Name));
                              }
                              
                          }
                          $scope.TransferDD = $scope.SelectList.value[0];
                          $scope.Account.Amount = "";
                          $scope.Account.Description = "";
                          $scope.HideDiv();
                          $scope.showTransfer.value = true;
                      }
                  }
              });
    }

    $scope.TransferFunds = function () {

        if ($scope.AccountBalance.balance < $scope.Account.Amount) {
            $scope.HideDiv();
            alert($scope.Account.Name + " Doesn't have sufficient funds for a transfer ");
            $scope.Account = {};
            $scope.ShowCreate();
        } else {

            $scope.transferFund =
                 {
                     "FloatAccountID": $scope.AccountID,
                     "FundsTo": $scope.TransferDD.ID,
                     "Amount": $scope.Account.Amount,
                     "Description": $scope.Account.Description
                 }
            $http({
                method: "POST",
                data: $scope.transferFund,
                url: "Account/Transfer"
            }).success(function (data, status, headers, config) {

                if (data.Status) {
                    $scope.Load();
                    $scope.HideDiv();
                    $scope.ShowCreate();
                    $scope.Account = {};
                    alert("Your transfer request has be processed");
                }
                else {
                    $scope.Errors = data.Data.ErrorMessages;
                }
            });
        }
    }

    $scope.PopulateRequestFunds = function (index) {
        $scope.Account.Name = $scope.Accounts[index].Name;
        $scope.Account.ID = $scope.Accounts[index].ID;
        $scope.HideDiv();
        $scope.showRequest.value = true;

    }

    $scope.RequestFunds = function () {
        $scope.requestFunds = {
            "ID": $scope.Account.ID,
            "Amount": $scope.Account.Amount,
            "Description": $scope.Account.Description,
        }

        $http({
            method: "POST",
            data: $scope.requestFunds,
            url: "Account/RequestFunds"
        }).success(function (data, status, headers, config) {

            if (data.Status) {
                $scope.Account =
                                 {
                                     "ID": "",
                                     "Name": "",
                                     "Amount": "",
                                     "AmountHeld": "",
                                     "UserNames": "",
                                     "Description": "",
                                     "Status": "",
                                     "Type": "",
                                     "DateCreated": "",
                                     "DateModified": " "
                                 }


                $scope.HideDiv()

                alert("You request has been raised. Thank you");
            }
            else {
                $scope.HideDiv()
                $scope.showTransfer.value = true;
                $scope.Errors = data.Data.ErrorMessages;
            }
        });

    }

    $scope.HideDiv = function () {
        $scope.showAddFunds.value = false;
        $scope.showCreate.value = false;
        $scope.showDetails.value = false;
        $scope.showTransfer.value = false;
        $scope.showRequest.value = false;
        $scope.showTransaction.value = false;
        $scope.Errors = {};
        $scope.addFund = {};
        $scope.Transactions = [];
    }
    //$scope.LoadByName = function () {

    //    debugger;
    //    $http({ method: "POST", data: $scope.Customer, url: "getCustomerByName" }).
    //success(function (data, status, headers, config) {
    //    $scope.Customers = data;

    //});
    //}



    //$scope.getColor = function (Amount) {
    //    if (Amount == 0) {
    //        return "";

    //    }
    //    else if (Amount > 100) {
    //        return "Blue";
    //    }
    //    else {
    //        return "Red";
    //    }

    //}
    //$scope.$watch("Customer.CustomerAmount", function () {
    //    $scope.Customer.CustomerAmountColor = $scope.
    //        getColor($scope.Customer.CustomerAmount);
    //});
    //$scope.Add = function () {
    //    // make a call to server to add data
    //    $http({ method: "POST", data: $scope.Customer, url: "Submit" }).
    //        success(function (data, status, headers, config) {
    //            $scope.Customers = data;
    //            // Load the collection of customer.
    //            $scope.Customer = {
    //                "CustomerCode": "",
    //                "CustomerName": "",
    //                "CustomerAmount": "",
    //                "CustomerAmountColor": ""
    //            };
    //        });
    //}


    //This function is used to load the data with the correct details
    $scope.Load();
}
