﻿function FloatTransaction($scope, $http, $filter) {
    //
    $scope.showDetails = { show: false };
    $scope.Errors = {};

    $scope.filterOptions = {
        filterText: '',
        useExternalFilter: true
    };

    $scope.filterName = '';
    $scope.filterCity = '';

    //$scope.$watch('filterName', function (value) {

    //    setFilterText();
    //});

    //$scope.$watch('filterCity', function (value) {

    //    setFilterText();
    //});

    $scope.Search = function setFilterText(value) {
        $scope.filterOptions.filterText = value;  //+ ";";//Type:' + $scope.filterCity + ";";
    }

    $scope.Reset = function setFilterText() {
        $scope.filterOptions.filterText = "";  //+ ";";//Type:' + $scope.filterCity + ";";
        $scope.filterName = '';
    }

    $scope.Transaction =
   {
       "ID": "",
       "Description": "",
       "Amount": "",
       "Status": "",
       "Type": "",
       "DateInitiated": "",
       "DateApproved": "",
       "InitiatedBy": "",
       "ApprovedBy": "",
       "Comments": "",
       "TypeColor": "",
       "UserId": ""
   }

    $scope.mySelections = [];
    $scope.Transactions = [];

    $scope.myData = [];

    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [50, 100, 150],
        pageSize: 50,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http({ method: "GET", url: "Transaction/GetTransactions" }).success(function (data, status, headers, config) {
                    var input = data.Transactions.filter(function (item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });
                    //  $scope.Transactions = data.Transactions;
                    $scope.UserId = data.UserId;
                    $scope.setPagingData(input, page, pageSize);
                });
            } else {
                $http({ method: "GET", url: "Transaction/GetTransactions" }).success(function (data, status, headers, config) {
                    $scope.UserId = data.UserId;
                    $scope.setPagingData(data.Transactions, page, pageSize);
                });
            }
        }, 100);
    };
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
                 $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    $scope.gridOptions = {
        data: 'myData',
        selectedItems: $scope.mySelections,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        multiSelect: false,
        columnDefs: [{ field: 'Description', displayName: 'Description' },
                         { field: 'Amount', displayName: 'Amount' },
                         { field: 'Status', displayName: 'Status' },
                         { field: 'Type', displayName: 'Type' },
                         { field: 'InitiatedBy', displayName: 'Initiated By' },
                         { field: 'DateInitiated', displayName: 'Date Initiated' },
                         { field: 'ApprovedBy', displayName: 'Approved By' },
                         { field: 'DateApproved', displayName: 'Date Approved' }],
    };

    $scope.$watch("Transactions", function () {
        for (var x = 0; x < $scope.Transactions.length; x++) {
            var transaction = $scope.Transactions[x];
            transaction.Index = x;
            transaction.TypeColor = $scope.GetColor(transaction.Type);
        }
    })

    $scope.GetColor = function (Type) {
        //if (Type == "Request") {
        //    return "red";
        //}
        //else (Type == "Transfer")
        //{
        //    return "green";
        //}

    }

    $scope.GetTransaction = function (index) {
        $scope.Transaction = $scope.Transactions[index];
        $scope.showDetails.show = true;
    }

    $scope.$watch("Transaction.Type", function () {
        $scope.Transaction.TypeColor = $scope.GetColor($scope.Transaction.Type);
    });

    $scope.HideDetails = function () {
        $scope.mySelections[0].__ng_selected__ = false;
    }

    $scope.ApproveTransaction = function (id, type) {
        $scope.approveTransaction =
            {
                "ID": id,
                "Comments": $scope.Transaction.Comments,
                "Status": type
            }

        // make a call to server to add data
        $http({ method: "POST", data: $scope.approveTransaction, url: "Transaction/Approve" }).
            success(function (data, status, headers, config) {
                if (data.Status) {
                    // $scope.Transactions = data.Data;
                    $scope.myData = data.Data.Transactions;
                    $scope.mySelections[0].__ng_selected__ = false;

                    alert("The transaction has been " + $scope.approveTransaction.Status);
                    $scope.approveTransaction = {};
                    $scope.Transaction.Comments = "";
                }
                else {
                    $scope.Errors = data.Data.ErrorMessages;
                }
            });
    }


    $scope.Load = function (id) {
        $http({ method: "GET", url: "Transaction/GetTransactions" }).
    success(function (data, status, headers, config) {
        $scope.Transactions = data.Transactions;
        $scope.UserId = data.UserId;
    });
    }

    $scope.LoadPending = function () {
        $http({ method: "GET", url: "Transaction/LoadData" }).
    success(function (data, status, headers, config) {
        $scope.Transactions = data
    });
    }

 }

//var ModalInstanceCtrl = function ($scope, $modalInstance) {

//    $scope.ok = function () {
//        $modalInstance.close();
//    };

//    $scope.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//};