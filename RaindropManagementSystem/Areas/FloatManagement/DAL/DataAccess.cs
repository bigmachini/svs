﻿using Dal;
using Models;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.FloatManagement.DAL
{
    public class DataAccess
    {
        //This method returns json for the account in the dbB
        public static List<JsonAccount> getFloatAccount(List<FloatAccount> list)
        {
            List<JsonAccount> jsonList = new List<JsonAccount>();
            ApplicationDbContext db = new ApplicationDbContext();

            foreach (var item in list)
            {
                JsonAccount data = new JsonAccount();
                var user = db.Users.ToList().Where(x => x.Id == item.UserID).SingleOrDefault();
                data.Create(item);
                if (user != null)
                    data.UserNames = user.FullNames;
                jsonList.Add(data);
            }

            return jsonList;
        }
    }
}