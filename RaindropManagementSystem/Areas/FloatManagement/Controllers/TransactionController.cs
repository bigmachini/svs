﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaindropManagementSystem.ViewModels;
using Microsoft.AspNet.Identity;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using RaindropManagementSystem.Helper;
using Dal;
using Models;
using RaindropManagementSystem.Generics;

namespace RaindropManagementSystem.Areas.FloatManagement.Controllers
{
    public class TransactionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FloatTransaction
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetTransactions()
        {
            var trans = await getTrans();
            return Json(trans, JsonRequestBehavior.AllowGet);
        }

        public async Task<TransactionResult<JsonTransaction>> getTrans()
        {
            List<JsonTransaction> dataReturned = new List<JsonTransaction>();
            List<FloatTransaction> transactions = new List<FloatTransaction>();
            var userId = User.Identity.GetUserId();
            TransactionResult<JsonTransaction> returnType = new TransactionResult<JsonTransaction>();
            returnType.UserId = userId;
            List<FloatAccount> floatAccount = await db.FloatAccounts.Where(x => x.UserID == userId).ToListAsync<FloatAccount>();


            if (!User.IsInRole("SuperAdmin"))
            {
                foreach (var account in floatAccount)
                {
                    transactions.AddRange(await db.FloatTransactions.Where(x => x.FloatAccountIn == account.ID || x.FloatAcountOut == account.ID || (x.InitiatedBy != userId && x.Type == EnumTransactionType.Request)).OrderByDescending(x => x.DateAuthorized).ToListAsync<FloatTransaction>());
                }
            }
            else
            {
                transactions = await db.FloatTransactions.OrderByDescending(x => x.DateAuthorized).ToListAsync<FloatTransaction>();
            }

            foreach (var item in transactions)
            {
                dataReturned.Add(new JsonTransaction().Create(item));
            }

            returnType.Transactions = dataReturned;
            return returnType;
        }

        [HttpPost]
        public async Task<ActionResult> Approve([Bind(Include = "ID,Comments,Status")]ApproveTransaction approveTrans)
        {
            ClientData result = new ClientData
            {
                Data = null,
                Status = true
            };

            if (ModelState.IsValid)
            {
                var floatTrans = await db.FloatTransactions.FindAsync(approveTrans.ID);

                if (approveTrans.Status == "Approved")
                {
                    FloatAccountDB.ApproveFunds(floatTrans);
                    floatTrans.Status = EnumStatus.Approved;
                }

                if (approveTrans.Status == "Declined")
                {
                    FloatAccountDB.DeclineFunds(floatTrans);
                    floatTrans.Status = EnumStatus.Declined;
                }

                floatTrans.Comments = approveTrans.Comments;
                floatTrans.DateApproved = HelperClass.DateTimeNow();
                floatTrans.ApprovedBy = User.Identity.GetUserId();

                db.Entry(floatTrans).State = EntityState.Modified;
                await db.SaveChangesAsync();
                result.Data = await getTrans();
            }
            else
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

      
        // GET: FloatTransaction/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloatTransaction floatTransaction = await db.FloatTransactions.FindAsync(id);
            if (floatTransaction == null)
            {
                return HttpNotFound();
            }
            return View(floatTransaction);
        }

        // POST: FloatTransaction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FloatTransaction floatTransaction = await db.FloatTransactions.FindAsync(id);
            db.FloatTransactions.Remove(floatTransaction);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Pending()
        {
            var dataReceived = await FloatAdminDB.GetTransactions();
            var userId = User.Identity.GetUserId();
            var result = FloatAdminDB.GetPending(dataReceived, "");
            ViewBag.UserName = FloatAdminDB.GetUserName(userId);
            return View(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
