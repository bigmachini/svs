﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using RaindropManagementSystem.Helper;
using Microsoft.AspNet.Identity;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Areas.FloatManagement.DAL;
using Dal;
using Models;
using RaindropManagementSystem.Filters;
using RaindropManagementSystem.Generics;

namespace RaindropManagementSystem.Areas.FloatManagement.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class AccountController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FloatAccount
        public ActionResult Index(int? status)
        {
            if (status != null)
            {
                ViewBag.ErrorMessage = "No Float account to move fund too";
            }

            return View();
        }

        public async Task<ActionResult> GetAccounts()
        {
            AccountResult<JsonAccount> returnType;

            List<JsonAccount> list = new List<JsonAccount>();
            var userId = User.Identity.GetUserId();

            if (User.IsInRole("SuperAdmin"))
            {
                var result = await db.FloatAccounts.ToListAsync();
                list = DataAccess.getFloatAccount(result);
                returnType = new AccountResult<JsonAccount>("SuperAdmin", list);
            }
            else
            {
                var result = await db.FloatAccounts.Where(x => x.UserID == userId && x.Status == true || x.FloatAccountType == FloatAccountType.Primary).ToListAsync();
                list = DataAccess.getFloatAccount(result);
                returnType = new AccountResult<JsonAccount>("Admin", list);
            }

            return Json(returnType, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetAccount(int Id)
        {
            JsonAccount account = new JsonAccount();
            var floatAccount = await db.FloatAccounts.FindAsync(Id);
            account.UserNames = account.GetUserName(floatAccount);
            return Json(account.Create(floatAccount), JsonRequestBehavior.AllowGet);
        }

        // GET: FloatAccount/Create
        public ActionResult Create()
        {
            FloatAccountViewModel favm = new FloatAccountViewModel();
            return View(favm);
        }

        // POST: FloatAccount/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<ActionResult> Create([Bind(Include = "Name,Description")] FloatAccountViewModel account)
        {
            ClientData result = new ClientData
            {
                Status = true,
                Data = null
            };

            if (ModelState.IsValid)
            {
                FloatAccount floatAccount = new FloatAccount()
                {
                    Amount = 0,
                    Description = account.Description,
                    FloatAccountType = FloatAccountType.Secondary,
                    Name = account.Name,
                    Status = false
                };

                floatAccount.UserID = "";


                floatAccount.DateCreated = HelperClass.DateTimeNow();
                floatAccount.DateModified = HelperClass.DateTimeNow();
                db.FloatAccounts.Add(floatAccount);
                await db.SaveChangesAsync();

                var accounts = await db.FloatAccounts.ToListAsync();
                var list = DataAccess.getFloatAccount(accounts);
                result.Data = new AccountResult<JsonAccount>("SuperAdmin", list);
            }
            else
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: FloatAccount/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloatAccount floatAccount = await db.FloatAccounts.FindAsync(id);

            FloatAccountEditViewModel faevm = new FloatAccountEditViewModel()
                {
                    Description = floatAccount.Description,
                    ID = floatAccount.ID,
                    Name = floatAccount.Name,
                    Status = floatAccount.Status
                };

            if (floatAccount == null)
            {
                return HttpNotFound();
            }
            return View(faevm);
        }

        // POST: FloatAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Description,Status")] FloatAccountEditViewModel faevm)
        {
            if (ModelState.IsValid)
            {
                var floatAccount = await db.FloatAccounts.FindAsync(faevm.ID);
                floatAccount.Name = faevm.Name;
                floatAccount.Status = faevm.Status;
                floatAccount.Description = faevm.Description;
                floatAccount.DateModified = HelperClass.DateTimeNow();

                db.Entry(floatAccount).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(faevm);
        }

        // GET: FloatAccount/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FloatAccount floatAccount = await db.FloatAccounts.FindAsync(id);
            if (floatAccount == null)
            {
                return HttpNotFound();
            }
            return View(floatAccount);
        }

        public async Task<ActionResult> Add(int id)
        {
            var floatAccount = await db.FloatAccounts.FindAsync(id);
            FloatAccountAdd faa = new FloatAccountAdd();
            faa.AccountName = floatAccount.Name;
            faa.ID = floatAccount.ID;
            return View(faa);
        }

        [HttpPost]
        public async Task<ActionResult> Add(JsonAccount account)
        {
            if (ModelState.IsValid)
            {
                FloatTransaction floatTransaction = new FloatTransaction()
                {
                    Amount = account.Amount,
                    FloatAccountIn = account.ID,
                    FloatAcountOut = account.ID,
                    Status = EnumStatus.Approved,
                    Description = account.Description,
                    Type = EnumTransactionType.Added
                };

                var userId = User.Identity.GetUserId();
                floatTransaction.ApprovedBy = userId;
                floatTransaction.InitiatedBy = userId;
                floatTransaction.DateApproved = HelperClass.DateTimeNow();
                floatTransaction.DateAuthorized = HelperClass.DateTimeNow();

                var result = FloatAccountDB.AddFundsSuper(account.ID, account.Amount);
                db.FloatTransactions.Add(floatTransaction);
                await db.SaveChangesAsync();

                var list = DataAccess.getFloatAccount(await db.FloatAccounts.ToListAsync<FloatAccount>());
                var returnType = new AccountResult<JsonAccount>("SuperAdmin", list);
                return Json(returnType, JsonRequestBehavior.AllowGet);
            }

            return View(account);
        }

        public async Task<ActionResult> Transfer(int id)
        {
            var floatAccount = await db.FloatAccounts.FindAsync(id);
            FloatAccountTransfer fat = new FloatAccountTransfer();
            fat.FloatAccountName = floatAccount.Name;
            fat.FloatAccountID = floatAccount.ID;

            var account = db.FloatAccounts.Where(x => x.Status == true && x.ID != id && x.FloatAccountType != FloatAccountType.Primary);

            if (account.Count() > 0)
            {
                ViewBag.FundsTo = new SelectList(account, "ID", "Name");
                return View(fat);
            }

            return RedirectToAction("Index", new { status = 10 });
        }

        [HttpPost]
        public async Task<ActionResult> Transfer([Bind(Include = "FloatAccountID,FundsTo,Amount,Description")]FloatAccountTransfer fat)
        {

            ClientData result = new ClientData
            {
                Status = true,
                Data = null
            };

            if (ModelState.IsValid)
            {
                FloatTransaction ft = new FloatTransaction()
                       {
                           Amount = fat.Amount,
                           FloatAcountOut = fat.FloatAccountID,
                           FloatAccountIn = fat.FundsTo,
                           Description = fat.Description
                       };

                ft.DateAuthorized = HelperClass.DateTimeNow();
                string userID = User.Identity.GetUserId();
                ft.InitiatedBy = userID;
                ft.Status = EnumStatus.Pending;
                ft.Type = EnumTransactionType.Transfer;
                db.FloatTransactions.Add(ft);
                await db.SaveChangesAsync();

                FloatAccountDB.TransferFunds(ft);
            }
            else
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetTransactions(int id)
        {
            List<JsonTransaction> dataReturned = new List<JsonTransaction>();
            var transactions = await db.FloatTransactions.Where(x => x.FloatAccountIn == id || x.FloatAcountOut == id).Take(10).ToListAsync<FloatTransaction>();
            //  var transactions = await db.FloatTransactions.OrderByDescending(x => x.DateAuthorized).Take(10).ToListAsync<FloatTransaction>();

            foreach (var item in transactions)
            {
                dataReturned.Add(new JsonTransaction().Create(item));
            }
            return Json(dataReturned, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> RequestFunds([Bind(Include = "ID,Amount,Description")]RequestFundViewModel rfvm)
        {

            ClientData result = new ClientData
            {
                Status = true,
                Data = null
            };

            if (ModelState.IsValid)
            {
                ErrorClass error = new ErrorClass();
                FloatTransaction floatTransaction = new FloatTransaction()
                {
                    Amount = rfvm.Amount,
                    Description = rfvm.Description,
                    FloatAccountIn = rfvm.ID,
                    FloatAcountOut = rfvm.ID,
                    Status = EnumStatus.Pending,
                    Type = EnumTransactionType.Request
                };
                var userId = User.Identity.GetUserId();

                floatTransaction.DateAuthorized = HelperClass.DateTimeNow();
                floatTransaction.InitiatedBy = userId;

                db.FloatTransactions.Add(floatTransaction);
                await db.SaveChangesAsync();


            }
            else
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Transactions(int id)
        {
            var transactions = await db.FloatTransactions.Where(x => x.FloatAccountIn == id || x.FloatAcountOut == id).ToListAsync();
            return View(transactions);
        }

        // POST: FloatAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FloatAccount floatAccount = await db.FloatAccounts.FindAsync(id);
            db.FloatAccounts.Remove(floatAccount);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
