﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;
using RaindropManagementSystem.Areas.DeviceManagement.ViewModels;

namespace RaindropManagementSystem.Areas.DeviceManagement.Controllers
{
    [Authorize]
    public class DevicesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DeviceManagement/Devices
        public ActionResult Index()
        {
            //var devices = db.Devices.Include(d => d.Certificate).Include(d => d.Sales);
            //return View(await devices.ToListAsync());
            return View();
        }

        public async Task<JsonResult> GetDevices()
        {
            var devices = await db.Devices.Include(d => d.Certificate).Include(d => d.Sales).ToListAsync();
            var deviceList = AutoMapper.Mapper.Map<List<DeviceListVM>>(devices);
            return Json(deviceList, JsonRequestBehavior.AllowGet);
        }

        [ActionName("GetDevice")]
        public async Task<ActionResult> GetDevices(int id)
        {
          var device =await db.Devices.Include(d => d.Certificate).Include(d => d.Sales).Where(x => x.ID == id).SingleOrDefaultAsync();
          //  var device = await db.Devices.Where(x => x.ID == id).SingleOrDefaultAsync();
            DeviceDetailVM result = null;
            if (device != null)
            {
                result = AutoMapper.Mapper.Map<DeviceDetailVM>(device);
            }
          //  Device d = new Device() { ID = device.ID, SalesID = device.SalesID, CertificateID = device.CertificateID, SerialNumber = device.SerialNumber };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: DeviceManagement/Devices/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = await db.Devices.FindAsync(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // GET: DeviceManagement/Devices/Create
        public ActionResult Create()
        {
            ViewBag.CertificateID = new SelectList(db.Certificates, "ID", "CertificateNumber");
            ViewBag.SalesID = new SelectList(db.Sales, "ID", "SalesAgentID");
            return View();
        }

        // POST: DeviceManagement/Devices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,CertificateID,SerialNumber,SalesID,status")] Device device)
        {
            if (ModelState.IsValid)
            {
                db.Devices.Add(device);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CertificateID = new SelectList(db.Certificates, "ID", "CertificateNumber", device.CertificateID);
            ViewBag.SalesID = new SelectList(db.Sales, "ID", "SalesAgentID", device.SalesID);
            return View(device);
        }

        // GET: DeviceManagement/Devices/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = await db.Devices.FindAsync(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.CertificateID = new SelectList(db.Certificates, "ID", "CertificateNumber", device.CertificateID);
            ViewBag.SalesID = new SelectList(db.Sales, "ID", "SalesAgentID", device.SalesID);
            return View(device);
        }

        // POST: DeviceManagement/Devices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,CertificateID,SerialNumber,SalesID,status")] Device device)
        {
            if (ModelState.IsValid)
            {
                db.Entry(device).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CertificateID = new SelectList(db.Certificates, "ID", "CertificateNumber", device.CertificateID);
            ViewBag.SalesID = new SelectList(db.Sales, "ID", "SalesAgentID", device.SalesID);
            return View(device);
        }

        // GET: DeviceManagement/Devices/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = await db.Devices.FindAsync(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // POST: DeviceManagement/Devices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Device device = await db.Devices.FindAsync(id);
            db.Devices.Remove(device);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
