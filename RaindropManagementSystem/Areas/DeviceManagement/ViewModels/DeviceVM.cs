﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.Areas.DeviceManagement.ViewModels
{
    public class DeviceListVM
    {
        public int ID { get; set; }
        public String AgentCompany { get; set; }
        public String SerialNumber { get; set; }
        public String CertificateNo { get; set; }
        public String Status { get; set; }
    }

    public class DeviceDetailVM
    {
        public String AgentCompany { get; set; }
        public String AgentName { get; set; }
        public String SerialNumber { get; set; }
        public String CertificateNo { get; set; }
        public String Status { get; set; }
        public String SoldBy { get; set; }
        public String DateSold { get; set; }
        public String DateInstalled { get; set; }
    }
}