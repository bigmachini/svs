namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FloatTransactionCommnet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FloatTransactions", "Comments", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FloatTransactions", "Comments");
        }
    }
}
