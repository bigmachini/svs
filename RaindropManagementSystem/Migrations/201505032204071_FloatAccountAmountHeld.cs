namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FloatAccountAmountHeld : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FloatAccounts", "AmountHeld", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FloatAccounts", "AmountHeld");
        }
    }
}
