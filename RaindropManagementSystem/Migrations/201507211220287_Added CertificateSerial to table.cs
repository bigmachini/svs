namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCertificateSerialtotable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Certificates", "CertificateSerialNo", c => c.String());
            AddColumn("dbo.Certificates", "CertificateNo", c => c.String());
            DropColumn("dbo.Certificates", "CertificateNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Certificates", "CertificateNumber", c => c.String());
            DropColumn("dbo.Certificates", "CertificateNo");
            DropColumn("dbo.Certificates", "CertificateSerialNo");
        }
    }
}
