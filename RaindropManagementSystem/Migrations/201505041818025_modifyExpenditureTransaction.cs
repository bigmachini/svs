namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifyExpenditureTransaction : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExpenditureTransactions", "DateApproved", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExpenditureTransactions", "DateApproved", c => c.DateTime(nullable: false));
        }
    }
}
