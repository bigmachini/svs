namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpenditureTransactionComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpenditureTransactions", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExpenditureTransactions", "Comment");
        }
    }
}
