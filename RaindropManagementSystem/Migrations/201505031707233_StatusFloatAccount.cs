namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StatusFloatAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FloatAccounts", "Status", c => c.Boolean(nullable: false));
            AlterColumn("dbo.FloatAccounts", "UserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.FloatAccounts", "UserID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.FloatAccounts", new[] { "UserID" });
            AlterColumn("dbo.FloatAccounts", "UserID", c => c.String());
            DropColumn("dbo.FloatAccounts", "Status");
        }
    }
}
