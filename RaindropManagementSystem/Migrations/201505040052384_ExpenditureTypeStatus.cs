namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpenditureTypeStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExpenditureTransactions", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExpenditureTransactions", "Status");
        }
    }
}
