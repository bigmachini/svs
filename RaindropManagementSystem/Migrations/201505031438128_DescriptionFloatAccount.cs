namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DescriptionFloatAccount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExpenditureTransactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Amount = c.Double(nullable: false),
                        FloatAccountID = c.Int(nullable: false),
                        DateInitiated = c.DateTime(nullable: false),
                        DateAPproved = c.DateTime(nullable: false),
                        InitiatedBy = c.String(),
                        ApprovedBy = c.String(),
                        ExpenditureTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ExpenditureTypes", t => t.ExpenditureTypeID, cascadeDelete: true)
                .Index(t => t.ExpenditureTypeID);
            
            CreateTable(
                "dbo.ExpenditureTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FloatAccounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Amount = c.Double(nullable: false),
                        UserID = c.String(),
                        Description = c.String(),
                        FloatAccountType = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FloatTransactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Amount = c.Double(nullable: false),
                        DateAuthorized = c.DateTime(nullable: false),
                        DateApproved = c.DateTime(nullable: false),
                        InitiatedBy = c.String(),
                        ApprovedBy = c.String(),
                        FloatAccountIn = c.Int(nullable: false),
                        FloatAcountOut = c.Int(nullable: false),
                        PaymentStatusID_ID = c.Int(),
                        PaymentTypeID_ID = c.Int(),
                        FloatAccount_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PaymentStatus", t => t.PaymentStatusID_ID)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeID_ID)
                .ForeignKey("dbo.FloatAccounts", t => t.FloatAccount_ID)
                .Index(t => t.PaymentStatusID_ID)
                .Index(t => t.PaymentTypeID_ID)
                .Index(t => t.FloatAccount_ID);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FloatTransactions", "FloatAccount_ID", "dbo.FloatAccounts");
            DropForeignKey("dbo.FloatTransactions", "PaymentTypeID_ID", "dbo.PaymentTypes");
            DropForeignKey("dbo.FloatTransactions", "PaymentStatusID_ID", "dbo.PaymentStatus");
            DropForeignKey("dbo.ExpenditureTransactions", "ExpenditureTypeID", "dbo.ExpenditureTypes");
            DropIndex("dbo.FloatTransactions", new[] { "FloatAccount_ID" });
            DropIndex("dbo.FloatTransactions", new[] { "PaymentTypeID_ID" });
            DropIndex("dbo.FloatTransactions", new[] { "PaymentStatusID_ID" });
            DropIndex("dbo.ExpenditureTransactions", new[] { "ExpenditureTypeID" });
            DropTable("dbo.PaymentTypes");
            DropTable("dbo.FloatTransactions");
            DropTable("dbo.FloatAccounts");
            DropTable("dbo.ExpenditureTypes");
            DropTable("dbo.ExpenditureTransactions");
        }
    }
}
