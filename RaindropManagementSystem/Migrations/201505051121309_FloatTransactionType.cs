namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FloatTransactionType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FloatTransactions", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FloatTransactions", "Type");
        }
    }
}
