namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedCertificateModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Certificates", "AgentID", "dbo.AgentDetails");
            DropIndex("dbo.Certificates", new[] { "AgentID" });
            AlterColumn("dbo.Certificates", "DateIssued", c => c.DateTime());
            AlterColumn("dbo.Certificates", "AgentID", c => c.Int());
            CreateIndex("dbo.Certificates", "AgentID");
            AddForeignKey("dbo.Certificates", "AgentID", "dbo.AgentDetails", "ID");
            DropColumn("dbo.Certificates", "Approved");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Certificates", "Approved", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Certificates", "AgentID", "dbo.AgentDetails");
            DropIndex("dbo.Certificates", new[] { "AgentID" });
            AlterColumn("dbo.Certificates", "AgentID", c => c.Int(nullable: false));
            AlterColumn("dbo.Certificates", "DateIssued", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Certificates", "AgentID");
            AddForeignKey("dbo.Certificates", "AgentID", "dbo.AgentDetails", "ID", cascadeDelete: true);
        }
    }
}
