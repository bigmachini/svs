namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgentDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AgentID = c.String(maxLength: 128),
                        Location = c.String(),
                        AgentNumber = c.String(),
                        CompanyName = c.String(),
                        PhoneNumber = c.String(),
                        Active = c.Boolean(nullable: false),
                        UnitsInStock = c.Int(nullable: false),
                        ReorderLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.AgentID)
                .Index(t => t.AgentID);
            
            CreateTable(
                "dbo.Certificates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CertificateNumber = c.String(),
                        DateIssued = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        AgentID = c.Int(nullable: false),
                        Approved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AgentDetails", t => t.AgentID, cascadeDelete: true)
                .Index(t => t.AgentID);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BuyingPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgentDetailID = c.Int(nullable: false),
                        SalesAgentID = c.String(maxLength: 128),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AgentDetails", t => t.AgentDetailID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.SalesAgentID)
                .Index(t => t.ProductID)
                .Index(t => t.AgentDetailID)
                .Index(t => t.SalesAgentID);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CertificateID = c.Int(),
                        SerialNumber = c.String(),
                        SalesID = c.Int(nullable: false),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Certificates", t => t.CertificateID)
                .ForeignKey("dbo.Sales", t => t.SalesID, cascadeDelete: true)
                .Index(t => t.CertificateID)
                .Index(t => t.SalesID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        ProductDescription = c.String(),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitsInStock = c.Int(nullable: false),
                        UnitsOnOrder = c.Int(nullable: false),
                        ReorderLevel = c.Int(nullable: false),
                        Discontinued = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        OrderID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Double(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Orders", t => t.OrderID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.OrderID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        DateRequired = c.DateTime(),
                        DateShipped = c.DateTime(),
                        ShipVia = c.Int(nullable: false),
                        Freight = c.Decimal(precision: 18, scale: 2),
                        ShipperName = c.String(),
                        ShipperAddress = c.String(),
                        ShipperPostalCode = c.String(),
                        ShipperCountry = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TransactionStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InitiatedBy = c.String(),
                        DateInitiated = c.DateTime(nullable: false),
                        FinalizeBy = c.String(),
                        DateFinalized = c.DateTime(),
                        Comments = c.String(),
                        OrderID = c.Int(),
                        FloatTransactionID = c.Int(),
                        ExpenditureTransactionID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ExpenditureTransactions", t => t.ExpenditureTransactionID)
                .ForeignKey("dbo.FloatTransactions", t => t.FloatTransactionID)
                .ForeignKey("dbo.Orders", t => t.OrderID)
                .Index(t => t.OrderID)
                .Index(t => t.FloatTransactionID)
                .Index(t => t.ExpenditureTransactionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransactionStatus", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.TransactionStatus", "FloatTransactionID", "dbo.FloatTransactions");
            DropForeignKey("dbo.TransactionStatus", "ExpenditureTransactionID", "dbo.ExpenditureTransactions");
            DropForeignKey("dbo.Sales", "SalesAgentID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Sales", "ProductID", "dbo.Products");
            DropForeignKey("dbo.OrderDetails", "ProductID", "dbo.Products");
            DropForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.Devices", "SalesID", "dbo.Sales");
            DropForeignKey("dbo.Devices", "CertificateID", "dbo.Certificates");
            DropForeignKey("dbo.Sales", "AgentDetailID", "dbo.AgentDetails");
            DropForeignKey("dbo.Certificates", "AgentID", "dbo.AgentDetails");
            DropForeignKey("dbo.AgentDetails", "AgentID", "dbo.AspNetUsers");
            DropIndex("dbo.TransactionStatus", new[] { "ExpenditureTransactionID" });
            DropIndex("dbo.TransactionStatus", new[] { "FloatTransactionID" });
            DropIndex("dbo.TransactionStatus", new[] { "OrderID" });
            DropIndex("dbo.OrderDetails", new[] { "ProductID" });
            DropIndex("dbo.OrderDetails", new[] { "OrderID" });
            DropIndex("dbo.Devices", new[] { "SalesID" });
            DropIndex("dbo.Devices", new[] { "CertificateID" });
            DropIndex("dbo.Sales", new[] { "SalesAgentID" });
            DropIndex("dbo.Sales", new[] { "AgentDetailID" });
            DropIndex("dbo.Sales", new[] { "ProductID" });
            DropIndex("dbo.Certificates", new[] { "AgentID" });
            DropIndex("dbo.AgentDetails", new[] { "AgentID" });
            DropTable("dbo.TransactionStatus");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Products");
            DropTable("dbo.Devices");
            DropTable("dbo.Sales");
            DropTable("dbo.Certificates");
            DropTable("dbo.AgentDetails");
        }
    }
}
