namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangingQuantitySaleDecimalToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sales", "Quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sales", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
