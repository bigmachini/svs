namespace RaindropManagementSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modificationFloatTransaction : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FloatTransactions", "PaymentStatusID_ID", "dbo.PaymentStatus");
            DropForeignKey("dbo.FloatTransactions", "PaymentTypeID_ID", "dbo.PaymentTypes");
            DropIndex("dbo.FloatTransactions", new[] { "PaymentStatusID_ID" });
            DropIndex("dbo.FloatTransactions", new[] { "PaymentTypeID_ID" });
            AddColumn("dbo.FloatTransactions", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.FloatTransactions", "DateApproved", c => c.DateTime());
            DropColumn("dbo.FloatTransactions", "PaymentStatusID_ID");
            DropColumn("dbo.FloatTransactions", "PaymentTypeID_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FloatTransactions", "PaymentTypeID_ID", c => c.Int());
            AddColumn("dbo.FloatTransactions", "PaymentStatusID_ID", c => c.Int());
            AlterColumn("dbo.FloatTransactions", "DateApproved", c => c.DateTime(nullable: false));
            DropColumn("dbo.FloatTransactions", "Status");
            CreateIndex("dbo.FloatTransactions", "PaymentTypeID_ID");
            CreateIndex("dbo.FloatTransactions", "PaymentStatusID_ID");
            AddForeignKey("dbo.FloatTransactions", "PaymentTypeID_ID", "dbo.PaymentTypes", "ID");
            AddForeignKey("dbo.FloatTransactions", "PaymentStatusID_ID", "dbo.PaymentStatus", "ID");
        }
    }
}
