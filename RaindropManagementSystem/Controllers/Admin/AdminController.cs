﻿using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Data.Entity;
using RaindropManagementSystem.Helper;
using RaindropManagementSystem.Areas.FloatManagement.ViewModels;
using Dal;
using Models;
using RaindropManagementSystem.Areas.ExpenditureManagement.ViewModels;

namespace RaindropManagementSystem.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public string UserId { get; set; }

        public AdminController()
        {

        }

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> FloatAccount()
        {
            UserId = User.Identity.GetUserId();
            var floatAccount = await db.FloatAccounts.Where(x => x.UserID == UserId && x.Status == true || x.FloatAccountType == FloatAccountType.Primary).ToListAsync();
            return View(floatAccount);
        }

        public async Task<ActionResult> Pending()
        {
            string userId = User.Identity.GetUserId();
            var pending = await db.ExpenditureTransactions.Where(x => x.Status == ExpenditureStatus.Pending && x.InitiatedBy != userId).ToListAsync();
            return View(pending);
        }

        public ActionResult Approve(int id)
        {
            AdminApproveViewModel aavm = AdminDB.CreateApproveModel(id);
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name", aavm.ExpenditureTypeID);
            return View(aavm);
        }

        [HttpPost]
        public async Task<ActionResult> Approve([Bind(Include = "ID,Comment,Status")]AdminApproveViewModel aavm)
        {
            if (ModelState.IsValid)
            {
                var expenditureTransaction = await db.ExpenditureTransactions.FindAsync(aavm.ID);

                switch (aavm.Status)
                {
                    case ExpenditureStatus.Approved:
                        {
                            var userId = User.Identity.GetUserId();
                            expenditureTransaction.ApprovedBy = userId;
                            expenditureTransaction.Status = ExpenditureStatus.Approved;
                            expenditureTransaction.DateApproved = HelperClass.DateTimeNow();

                            db.Entry(expenditureTransaction).State = EntityState.Modified;
                            await db.SaveChangesAsync();

                            //  FloatAccountDB.ApproveFunds(expenditureTransaction.FloatAccountID, expenditureTransaction.Amount);

                            return RedirectToAction("Pending");
                        }
                    case ExpenditureStatus.Declined:
                        {
                            var userId = User.Identity.GetUserId();
                            expenditureTransaction.ApprovedBy = userId;
                            expenditureTransaction.Status = ExpenditureStatus.Declined;
                            expenditureTransaction.DateApproved = HelperClass.DateTimeNow();

                            db.Entry(expenditureTransaction).State = EntityState.Modified;
                            await db.SaveChangesAsync();

                            FloatAccountDB.AddFunds(expenditureTransaction.FloatAccountID, expenditureTransaction.Amount);

                            return RedirectToAction("Pending");
                        }
                    case ExpenditureStatus.Pending:
                        break;

                    default:
                        break;
                }


            }

            aavm = AdminDB.CreateApproveModel(aavm.ID);
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name", aavm.ExpenditureTypeID);
            return View(aavm);
        }




        [HttpPost]
        public async Task<ActionResult> RequestFunds([Bind(Include = "ID,Name,Amount,Description")]RequestFundViewModel rfvm)
        {

            if (ModelState.IsValid)
            {
                FloatTransaction floatTransaction = new FloatTransaction()
                {
                    Amount = rfvm.Amount,
                    Description = rfvm.Description,
                    FloatAccountIn = rfvm.ID,
                    FloatAcountOut = rfvm.ID,
                    Status = EnumStatus.Pending,
                    Type = EnumTransactionType.Request
                };
                var userId = User.Identity.GetUserId();

                floatTransaction.DateAuthorized = HelperClass.DateTimeNow();
                floatTransaction.InitiatedBy = userId;

                db.FloatTransactions.Add(floatTransaction);
                await db.SaveChangesAsync();

                return RedirectToAction("FloatAccount");

            }

            return View(rfvm);
        }

        public async Task<ActionResult> FloatRequested()
        {
            return View(await db.FloatTransactions.Where(x => x.Type == EnumTransactionType.Request).ToListAsync());
        }

        public async Task<ActionResult> FloatPending()
        {
            var userId = User.Identity.GetUserId();
            return View(await db.FloatTransactions.Where(x => x.Status == EnumStatus.Pending && x.InitiatedBy != userId).ToListAsync());
        }

        public async Task<ActionResult> FloatTransactions()
        {
            var userId = User.Identity.GetUserId();
            return View(await db.FloatTransactions.ToListAsync());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}