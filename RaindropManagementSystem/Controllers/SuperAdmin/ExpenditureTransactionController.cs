﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Helper;
using Dal;
using Models;

namespace RaindropManagementSystem.Controllers
{
    public class ExpenditureTransactionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ExpenditureTransaction
        public async Task<ActionResult> Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var transactions = await db.ExpenditureTransactions.Include(x => x.ExpenditureType).ToListAsync();

            var result = ExpenditureDB.GetTransactions(transactions);

            return View(result);
        }

        // GET: ExpenditureTransaction/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureTransaction expenditureTransaction = await db.ExpenditureTransactions.FindAsync(id);
            if (expenditureTransaction == null)
            {
                return HttpNotFound();
            }

            var expenditure = ExpenditureDB.GetTransactionDetail((int)id);

            return View(expenditure);
        }

        // GET: ExpenditureTransaction/Create
        public ActionResult Create()
        {
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name");
            return View();
        }

        // POST: ExpenditureTransaction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Description,Amount,FloatAccountID,DateInitiated,DateAPproved,InitiatedBy,ApprovedBy,ExpenditureTypeID")] ExpenditureTransaction expenditureTransaction)
        {
            if (ModelState.IsValid)
            {
                db.ExpenditureTransactions.Add(expenditureTransaction);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name", expenditureTransaction.ExpenditureTypeID);
            return View(expenditureTransaction);
        }

        // GET: ExpenditureTransaction/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureTransaction expenditureTransaction = await db.ExpenditureTransactions.FindAsync(id);
            if (expenditureTransaction == null)
            {
                return HttpNotFound();
            }
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name", expenditureTransaction.ExpenditureTypeID);
            return View(expenditureTransaction);
        }

        // POST: ExpenditureTransaction/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Description,Amount,FloatAccountID,DateInitiated,DateAPproved,InitiatedBy,ApprovedBy,ExpenditureTypeID")] ExpenditureTransaction expenditureTransaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(expenditureTransaction).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ExpenditureTypeID = new SelectList(db.ExpenditureTypes, "ID", "Name", expenditureTransaction.ExpenditureTypeID);
            return View(expenditureTransaction);
        }

        // GET: ExpenditureTransaction/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureTransaction expenditureTransaction = await db.ExpenditureTransactions.FindAsync(id);
            if (expenditureTransaction == null)
            {
                return HttpNotFound();
            }
            return View(expenditureTransaction);
        }

        // POST: ExpenditureTransaction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ExpenditureTransaction expenditureTransaction = await db.ExpenditureTransactions.FindAsync(id);
            db.ExpenditureTransactions.Remove(expenditureTransaction);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
