﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Helper;
using Microsoft.AspNet.Identity;
using RaindropManagementSystem.Generics;

namespace RaindropManagementSystem.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        // GET: Product/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public async Task<ActionResult> GetProducts()
        {
            return Json(await Products(), JsonRequestBehavior.AllowGet);
        }

        // GET: Product/Create
        private async Task<Object> Products()
        {
            var UserId = User.Identity.GetUserId();
            string roleName = "SuperAdmin";

            if (!User.IsInRole("SuperAdmin"))
            {
                roleName = "Admin";
            }
            var products = await db.Products.ToListAsync();
            var jsonProducts = AutoMapper.Mapper.Map<List<JsonProduct>>(products);
            return new TransactionResult<JsonProduct>(UserId, jsonProducts, roleName);
        }

        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
      //  [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,Description,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel")] ProductCreateVM productCreate)
        {
            ClientData result = new ClientData
            {
                Status = false,
                Data = null
            };

            if (ModelState.IsValid)
            {
                //var destinationObject = AutoMapper.Mapper.Map<DestinatationClass>(sourceObject);
                var product = AutoMapper.Mapper.Map<Product>(productCreate);
                db.Products.Add(product);
                await db.SaveChangesAsync();
               
                result.Status = true;
                result.Data = await Products();
            }


            if (result.Status == false)
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
       [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Description,ReorderLevel")] ProductEdit productEdit)
        {
            ClientData result = new ClientData
            {
                Status = false,
                Data = null
            };

            if (ModelState.IsValid)
            {
                var product = await db.Products.FindAsync(productEdit.ID);
                product.ProductDescription = productEdit.Description;
                product.ReorderLevel = productEdit.ReorderLevel;

                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();

                result.Status = true;
                result.Data = await Products();
            }

            if (result.Status == false)
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await db.Products.FindAsync(id);
            db.Products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
