﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Helper;
using Dal;
using Models;

namespace RaindropManagementSystem.Controllers
{
    public class ExpenditureTypeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ExpenditureType
        public async Task<ActionResult> Index()
        {
            return View(await db.ExpenditureTypes.ToListAsync());
        }

        // GET: ExpenditureType/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureType expenditureType = await db.ExpenditureTypes.FindAsync(id);
            if (expenditureType == null)
            {
                return HttpNotFound();
            }
            return View(expenditureType);
        }

        // GET: ExpenditureType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExpenditureType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,Description")] ExpenditureTypeViewModel etvm)
        {
            if (ModelState.IsValid)
            {
                ExpenditureType expenditureType = new ExpenditureType()
                {
                    Description = etvm.Description,
                    Name = etvm.Name
                };

                expenditureType.DateCreated = HelperClass.DateTimeNow();
                expenditureType.DateModified = HelperClass.DateTimeNow();

                db.ExpenditureTypes.Add(expenditureType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(etvm);
        }

        // GET: ExpenditureType/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureType expenditureType = await db.ExpenditureTypes.FindAsync(id);

            if (expenditureType == null)
            {
                return HttpNotFound();
            }

            ExpenditureTypeEditViewModel etevm = new ExpenditureTypeEditViewModel()
            {
                ID = expenditureType.ID,
                Name = expenditureType.Name,
                Description = expenditureType.Description
            };


            return View(etevm);
        }

        // POST: ExpenditureType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Description,DateCreated,DateModified")] ExpenditureTypeEditViewModel etevm)
        {
            if (ModelState.IsValid)
            {
                var expenditureType = await db.ExpenditureTypes.FindAsync(etevm.ID);

                expenditureType.Description = etevm.Description;
                expenditureType.Name = etevm.Name;
                expenditureType.DateModified = HelperClass.DateTimeNow();
                db.Entry(expenditureType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(etevm);
        }

        // GET: ExpenditureType/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenditureType expenditureType = await db.ExpenditureTypes.FindAsync(id);
            if (expenditureType == null)
            {
                return HttpNotFound();
            }
            return View(expenditureType);
        }

        // POST: ExpenditureType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ExpenditureType expenditureType = await db.ExpenditureTypes.FindAsync(id);
            db.ExpenditureTypes.Remove(expenditureType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
