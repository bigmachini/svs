﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;

namespace RaindropManagementSystem.Controllers.SuperAdmin
{
    [Authorize(Roles="SuperAdmin")]
    public class ActionLogController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ActionLog
        public async Task<ActionResult> Index()
        {
            return View(await db.ActionLogs.ToListAsync());
        }

        // GET: ActionLog/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionLog actionLog = await db.ActionLogs.FindAsync(id);
            if (actionLog == null)
            {
                return HttpNotFound();
            }
            return View(actionLog);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
