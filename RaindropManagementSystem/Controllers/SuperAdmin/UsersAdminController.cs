﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RaindropManagementSystem.ViewModels;
using PagedList;
using System.Diagnostics;
using Dal;
using Models;


namespace RaindropManagementSystem.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class UsersAdminController : Controller
    {
        public UsersAdminController()
        {
        }

        public UsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /Users/
        public async Task<ActionResult> Index(int? page, int? status)
        {
           
            if(status != null)
            {
                @ViewBag.ErrorMessage = "No float account to link.";
            }

            var result = await UserManager.Users.ToListAsync();

            var userId = User.Identity.GetUserId();
            var user = UserManager.Users.Where( x => x.Id == userId).SingleOrDefault();

            if(user.UserName.ToLower() != "bigmachini".ToLower())
            {
                result = result.Where(x => x.UserName.ToLower() != "Bigmachini".ToLower()).ToList();
            }
            else
            {
                result = result.ToList();
            }
           
            //   return View();
            int pageSize = 30;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }




        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);


            return View(user);
        }

        //
        // GET: /Users/Create
        public async Task<ActionResult> Create()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            //Get the list of Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }


        //
        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new ApplicationUser
                    {
                        UserName = userViewModel.UserName,
                        Email = userViewModel.Email,
                        FirstName = userViewModel.FirstName,
                        MiddleName = userViewModel.MiddleName,
                        LastName = userViewModel.LastName,
                        IDNumber = userViewModel.IDNumber,
                        PhoneNumber = userViewModel.PhoneNumber,
                        RoleSelected = selectedRoles[0]
                    };


                    //Then create
                    var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);


                    //Add User to the selected Roles 
                    if (adminresult.Succeeded)
                    {
                        if (selectedRoles != null)
                        {
                            var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);

                            if (!result.Succeeded)
                            {
                                ModelState.AddModelError("", result.Errors.First());
                                ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                                return View();
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", adminresult.Errors.First());
                        ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                        return View();

                    }
                    return RedirectToAction("Index");

                }
                catch (NullReferenceException ex)
                {
                    Debug.WriteLine("Processor Usage" + ex.Message);
                }
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        //
        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);
            var user1 = new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                IDNumber = user.IDNumber,
                PhoneNumber = user.PhoneNumber,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })

            };

            return View(user1);
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Email,Id,FirstName,MiddleName,LastName,IDNumber,Books,Locations,PhoneNumber,LocationID")] EditUserViewModel editUser,

            params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                user.Email = editUser.Email;
                user.FirstName = editUser.FirstName;
                user.MiddleName = editUser.MiddleName;
                user.LastName = editUser.LastName;
                user.IDNumber = editUser.IDNumber;
                user.PhoneNumber = editUser.PhoneNumber;
                user.RoleSelected = selectedRole[0];

                var userRoles = await UserManager.GetRolesAsync(user.Id);



                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }


                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");

            return View();
        }

        //
        // GET: /Users/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpGet]
        public ActionResult Assign(string id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var account = db.FloatAccounts.Where(x => x.Status == false);

            if (account.Count() > 0)
            {
                ViewBag.FloatAccountID = new SelectList(account, "ID", "Name");
                AssignUserViewModel auvm = new AssignUserViewModel();
                auvm.UserID = id;
                return View(auvm);
            }
            else
            {
                ModelState.AddModelError("", "No accounts to link");
                return RedirectToAction("Index", new { status = 3});

            }
        }

        [HttpPost]
        public ActionResult Assign([Bind(Include = "UserID,FloatAccountID")] AssignUserViewModel auvm)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            if (ModelState.IsValid)
            {
                FloatAccount floatAccount = db.FloatAccounts.Find(auvm.FloatAccountID);
                floatAccount.UserID = auvm.UserID;
                floatAccount.Status = true;

                db.Entry(floatAccount).State = EntityState.Modified;
                db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FloatAccountID = new SelectList(db.FloatAccounts, "ID", "Name");
            return View();
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}