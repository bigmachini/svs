﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dal;
using Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using RaindropManagementSystem.ViewModels;
using RaindropManagementSystem.Helper;

namespace RaindropManagementSystem.Controllers.SuperAdmin
{
    public class AgentDetailController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public AgentDetailController()
        {
        }

        public AgentDetailController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: AgentDetail
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        public ActionResult AddDetails()
        {
            return PartialView("_AddDetails");
        }

        // GET: AgentDetail/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    AgentDetails agentDetails = await db.AgentDetails.FindAsync(id);
        //    if (agentDetails == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(agentDetails);
        //}

        // GET: AgentDetail/Create


        // POST: AgentDetail/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FirstName,MiddleName,LastName,IDNumber,PhoneNumber,Email,Username,Password,Location,AgentNumber,CompanyName,PhoneNumber2")] AgentVM agentVM)
        {
            ClientData result = new ClientData
            {
                Status = false,
                Data = null
            };

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = agentVM.UserName,
                    Email = agentVM.Email,
                    FirstName = agentVM.FirstName,
                    MiddleName = agentVM.MiddleName,
                    LastName = agentVM.LastName,
                    IDNumber = agentVM.IDNumber,
                    PhoneNumber = agentVM.PhoneNumber,
                    RoleSelected = "Agent"
                };

                //Creating agent
                var agentResult = await UserManager.CreateAsync(user, agentVM.Password);


                //Adding role to agent 
                if (agentResult.Succeeded)
                {
                    string[] selectedRoles = { "Agent" };
                    var rolesResult = await UserManager.AddToRolesAsync(user.Id, selectedRoles);

                    if (rolesResult.Succeeded)
                    {
                        var agentDetail = AutoMapper.Mapper.Map<AgentDetails>(agentVM);
                        agentDetail.AgentID = user.Id;

                        db.AgentDetails.Add(agentDetail);
                        await db.SaveChangesAsync();

                        result.Status = true;

                    }
                    else
                    {
                        ModelState.AddModelError("", rolesResult.Errors.First());
                    }
                }
                else
                {
                    ModelState.AddModelError("", agentResult.Errors.First());
                }
            }

            if (result.Status == false)
            {
                ErrorClass Err = new ErrorClass();
                foreach (var modelState in ModelState)
                {
                    foreach (var error in modelState.Value.Errors)
                    {
                        string errorMessage = error.ErrorMessage.ToString();
                        Err.ErrorMessages.Add(errorMessage);
                    }
                }

                result.Status = false;
                result.Data = Err;
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }


        // POST: AgentDetail/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,UserID,Location,AgentNumber,CompanyName,Active,UnitsInStock,ReorderLevel")] AgentDetails agentDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(agentDetails).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(agentDetails);
        }

        [HttpPost]
        public async Task<JsonResult> EmailAlreadyExistsAsync(string email)
        {
            bool returnValue = false;

            if (email != null)
            {
                var result = await UserManager.FindByEmailAsync(email);
                returnValue = result == null ? true : false;
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UserAlreadyExistsAsync(string username)
        {
            bool returnValue = false;

            if (username != null)
            {
                var result = await db.Users.Where(x => x.UserName == username).FirstOrDefaultAsync();
                returnValue = result == null ? true : false;
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetAgentDetails()
        { 
            var agentDetails = await db.AgentDetails.Include(x => x.Agent).ToListAsync();
            var agentVM = AutoMapper.Mapper.Map<List<AgentVM>>(agentDetails);
            return Json(agentVM, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetDetails(int id)
        {
            var agent = await db.AgentDetails.Where(x => x.ID == id).Include(x => x.Agent).FirstOrDefaultAsync();
            var agentVM = AutoMapper.Mapper.Map<AgentDetailsVM>(agent);
            return Json(agentVM, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
