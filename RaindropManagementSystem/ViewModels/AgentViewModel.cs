﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.ViewModels
{
    public class AgentVM
    {
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        //added fields
        [Display(Name = "First Name")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name(s)")]
        public string MiddleName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "ID Number")]
        [Required]
        public string IDNumber { get; set; }

        [Display(Name = "Phone No")]
        [Required]
     //   [RegularExpression(@"^[0][7]\d{8}$", ErrorMessage = "Phone format should be 07XX XXXXXX")]
        [StringLength(12, ErrorMessage = "The {0} should be at {1} digits", MinimumLength = 10)]
        public string PhoneNumber { get; set; }


        [Required]
        [Display(Name="Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }
        [Required]
        [Display(Name = "Agent Number")]
        public string AgentNumber { get; set; }
        [Required]
        [Display(Name = "Company Phone Number")]
        public string PhoneNumber2 { get; set; }

        [Required]
        [Display(Name = "Agent Status")]
        public bool Active { get; set; }

        public int ID { get; set; }

    }

    public class AgentDetailsVM
    {
        public string AgentNames { get; set; }
        public string CompanyName { get; set; }
        public string Location { get; set; }
        public string AgentNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int UnitsInStock { get; set; }
        public int ReorderLevel { get; set; }
        public string Active { get; set; }
    }
}