﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.ViewModels
{
    public class ExpenditureTypeViewModel
    {
        [Required]
        [Display(Name="Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }

    public class ExpenditureTypeEditViewModel : ExpenditureTypeViewModel
    {
        public int ID { get; set; }
        
    } 


}