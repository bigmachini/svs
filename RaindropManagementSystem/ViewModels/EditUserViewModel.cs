﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace RaindropManagementSystem.ViewModels
{
    public class EditUserViewModel
    {
        public EditUserViewModel()
        {

        }

        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }
        public string FirstName { get; set; }

        [Display(Name = "Middle Name(s)")]
        public string MiddleName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "ID Number")]
        [Required]
        public string IDNumber { get; set; }

        [Display(Name = "Phone No")]
        [Required]
        [RegularExpression(@"^[0][7]\d{8}$", ErrorMessage = "Phone format should be 07XX XXXXXX")]
        [StringLength(10, ErrorMessage = "The {0} should be at {1} digits", MinimumLength = 10)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Full Names")]
        public string FullNames
        {
            get
            {
                string middleName = string.IsNullOrWhiteSpace(this.MiddleName) ? "" : this.MiddleName;
                return string.Format("{0} {1} {2}", this.FirstName, middleName, LastName);
            }
        }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }

    public class AssignUserViewModel
    {
        public string UserID { get; set; }
        public int FloatAccountID { get; set; }
    }
}
