﻿using Models;
using RaindropManagementSystem.Areas.CertificateManagement.ViewModels;
using RaindropManagementSystem.Areas.DeviceManagement.ViewModels;
using RaindropManagementSystem.Areas.SalesManagement.ViewModels;
using RaindropManagementSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaindropManagementSystem.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            //AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();

            //Mapping a productCreateVM to a product from the view
            AutoMapper.Mapper.CreateMap<ProductCreateVM, Product>()
              .ForMember(dest => dest.UnitPrice,
              opt => opt.MapFrom(src => System.Convert.ToDecimal(src.UnitPrice)))
              .ForMember(dest => dest.Discontinued,
              opt => opt.MapFrom(src => false))
              .ForMember(dest => dest.ProductName,
              opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.ProductDescription,
              opt => opt.MapFrom(src => src.Description));

            //mapping a product to a JsonProct for the view to consume
            AutoMapper.Mapper.CreateMap<Product, JsonProduct>()
                .ForMember(dest => dest.UnitPrice,
                opt => opt.MapFrom(src => System.Convert.ToDouble(src.UnitPrice)))
                .ForMember(dest => dest.Discontinued,
                opt => opt.MapFrom(src => String.Format("{0}", src.Discontinued.ToString())))
                 .ForMember(dest => dest.Name,
              opt => opt.MapFrom(src => src.ProductName))
              .ForMember(dest => dest.Description,
              opt => opt.MapFrom(src => src.ProductDescription));

            //mapping agentVM to ApplicationUser from the View to create a new user
            AutoMapper.Mapper.CreateMap<AgentVM, ApplicationUser>()
            .ForMember(dest => dest.RoleSelected,
               opt => opt.MapFrom(src => "Agent"));

            //Mapping AgentVM to AgentDetails to create the object from input from the view
            AutoMapper.Mapper.CreateMap<AgentVM, AgentDetails>()
           .ForMember(dest => dest.PhoneNumber,
              opt => opt.MapFrom(src => src.PhoneNumber2))
               .ForMember(dest => dest.Active,
              opt => opt.MapFrom(src => true));

            //mapping AgentDetails to AgentVM to be consumer by the view
            AutoMapper.Mapper.CreateMap<AgentDetails, AgentVM>()
                .ForMember(dest => dest.PhoneNumber2,
                opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(dest => dest.PhoneNumber,
                opt => opt.MapFrom(src => src.Agent.PhoneNumber))
                .ForMember(dest => dest.FirstName,
                opt => opt.MapFrom(src => src.Agent.FirstName))
                .ForMember(dest => dest.MiddleName,
                opt => opt.MapFrom(src => src.Agent.MiddleName))
                .ForMember(dest => dest.LastName,
                opt => opt.MapFrom(src => src.Agent.LastName))
                .ForMember(dest => dest.IDNumber,
                opt => opt.MapFrom(src => src.Agent.IDNumber))
                .ForMember(dest => dest.Email,
                opt => opt.MapFrom(src => src.Agent.Email))
                .ForMember(dest => dest.UserName,
                opt => opt.MapFrom(src => src.Agent.UserName));

            //mapping for the agent details from the server.
            AutoMapper.Mapper.CreateMap<AgentDetails, AgentDetailsVM>()
                .ForMember(dest => dest.Active,
                    opt => opt.MapFrom(src => src.Active.ToString()))
                    .ForMember(dest => dest.AgentNames,
                opt => opt.MapFrom(src => String.Format(" {0} {1}", src.Agent.FirstName, src.Agent.LastName)));

            //mapping agent to saleagentvm
            AutoMapper.Mapper.CreateMap<AgentDetails, AgentSaleVM>()
                .ForMember(dest => dest.ID,
                opt => opt.MapFrom(src => src.ID))
                .ForMember(dest => dest.CompanyName,
                opt => opt.MapFrom(src => src.CompanyName));

            //mapping from createsale to sale
            AutoMapper.Mapper.CreateMap<SalesCreateVM, Sales>()
                .ForMember(dest => dest.AgentDetailID,
                opt => opt.MapFrom(src => src.AgentID))
                .ForMember(dest => dest.BuyingPrice,
                opt => opt.MapFrom(src => System.Convert.ToDecimal(src.BuyingPrice)))
                .ForMember(x => x.Devices, opt => opt.Ignore());


            //Mapping from Sales to SalesList
            AutoMapper.Mapper.CreateMap<Sales, SalesList>()
                .ForMember(dest => dest.ProductName,
                opt => opt.MapFrom(src => src.Product.ProductName))
                .ForMember(dest => dest.BuyingPrice,
                opt => opt.MapFrom(src => System.Convert.ToDouble(src.BuyingPrice)))
                .ForMember(dest => dest.Total,
                    opt => opt.MapFrom(src => (src.Quantity * src.BuyingPrice)))
                .ForMember(dest => dest.CompanyName,
                    opt => opt.MapFrom(src => src.AgentDetail.CompanyName))
                .ForMember(dest => dest.SoldBy,
                opt => opt.MapFrom(src => src.SalesAgent.FullNames))
                .ForMember(dest => dest.Timestamp,
                opt => opt.MapFrom(src => String.Format("{0:dd/MM/yy H:mm}", src.Timestamp)));

            //Mapping from Device to DeviceList
            AutoMapper.Mapper.CreateMap<Device, DeviceListVM>()
                .ForMember(dest => dest.ID,
                opt => opt.MapFrom(src => src.ID))
                .ForMember(dest => dest.AgentCompany,
                opt => opt.MapFrom(src => src.Sales.AgentDetail.CompanyName))
                .ForMember(dest => dest.CertificateNo,
                opt => opt.MapFrom(src => String.IsNullOrWhiteSpace(src.Certificate.CertificateSerialNo) ? "" : src.Certificate.CertificateSerialNo))
               .ForMember(dest => dest.SerialNumber,
               opt => opt.MapFrom(src => src.SerialNumber))
               .ForMember(dest => dest.Status,
               opt => opt.MapFrom(src => src.status.ToString().ToUpper()));

            AutoMapper.Mapper.CreateMap<Device, DeviceDetailVM>()
                .ForMember(dest => dest.Status,
                opt => opt.MapFrom(src => src.status.ToString().ToUpper()))
                .ForMember(dest => dest.SerialNumber,
                opt => opt.MapFrom(src => src.SerialNumber))
                .ForMember(dest => dest.SoldBy,
                opt => opt.MapFrom(src => src.Sales.SalesAgent.FullNames))
                .ForMember(dest => dest.DateSold,
                opt => opt.MapFrom(src =>  String.Format("{0:dd/MM/yy H:mm}",src.Sales.Timestamp)))
                .ForMember(dest => dest.DateInstalled,
                opt => opt.MapFrom(src => src.Certificate == null ? null : String.Format("{0:dd/MM/yy H:mm}", src.Certificate.DateIssued)))
                .ForMember(dest => dest.AgentName, 
                opt => opt.MapFrom(src => src.Sales.AgentDetail.Agent.FullNames))
             .ForMember(dest => dest.CertificateNo,
                opt => opt.MapFrom(src => String.IsNullOrWhiteSpace(src.Certificate.CertificateSerialNo) ? "" : src.Certificate.CertificateSerialNo))
                .ForMember(dest => dest.AgentCompany,
                opt => opt.MapFrom(src => src.Sales.AgentDetail.CompanyName));

            //Create Mapper for CertListVM
            AutoMapper.Mapper.CreateMap<Certificate, CertListVM>()
                .ForMember(dest => dest.ID,
                opt => opt.MapFrom(src => src.ID))
                .ForMember(dest => dest.AgentName,
                opt => opt.MapFrom(src => (src.Agent != null) ? src.Agent.CompanyName : ""))
                .ForMember(dest => dest.SerialNo,
                opt => opt.MapFrom(src => src.CertificateSerialNo.ToString()))
                 .ForMember(dest => dest.CertNo,
                opt => opt.MapFrom(src => src.CertificateNo.ToString()))
                .ForMember(dest => dest.DateIssued,
                opt => opt.MapFrom(src => (src.DateIssued != null) ? String.Format("{0:dd/MM/yy H:mm}", src.DateIssued) : ""))
                .ForMember(dest => dest.Status,
                opt => opt.MapFrom(src => src.Status.ToString().ToUpper()))
                ;
                
        }
    }
}

//public class SalesList
//{
//    public int ID { get; set; }
//    public string ProductName { get; set; }
//    public int Quantity { get; set; }
//    public double BuyingPrice { get; set; }
//    public double Total { get; set; }
//    public string CompanyName { get; set; }
//    public string SoldBy { get; set; }
//    public string Timestamp { get; set; }
//}
