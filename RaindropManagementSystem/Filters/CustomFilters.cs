﻿using Dal;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace RaindropManagementSystem.Filters
{

    //public sealed class ValidateCustomAntiForgeryTokenAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
    //    {
    //        if (actionContext == null)
    //        {
    //            throw new ArgumentNullException("actionContext");
    //        }
    //        var headers = actionContext.Request.Headers;
    //        var cookie = headers
    //            .GetCookies()
    //            .Select(c => c[AntiForgeryConfig.CookieName])
    //            .FirstOrDefault();
    //        var tokenFromHeader = headers.GetValues("X-XSRF-Token").FirstOrDefault();
    //        AntiForgery.Validate(cookie != null ? cookie.Value : null, tokenFromHeader);

    //        base.OnActionExecuting(actionContext);
    //    }
    //}


    public class LoggingFilter : ActionFilterAttribute, System.Web.Mvc.IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // TODO: Add your acction filter's tasks here

            // Log Action Filter Call
            ApplicationDbContext db = new ApplicationDbContext();
            // var userId =  filterContext. // User.Identity.GetUserId();
            var userId = filterContext.HttpContext.User.Identity.GetUserId();
            var user = db.Users.ToList().Where(x => x.Id == userId).SingleOrDefault();
            string userName = "";

            if (user != null)
            {
                userName = user.FullNames;
            }

            ActionLog log = new ActionLog()
            {
                Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                Action = filterContext.ActionDescriptor.ActionName + " (Logged By: Action Filter)",
                IP = filterContext.HttpContext.Request.UserHostAddress,
                Timestamp = filterContext.HttpContext.Timestamp,
                UserNames = userName
            };

            db.ActionLogs.Add(log);
            db.SaveChanges();

            this.OnActionExecuting(filterContext);
        }
    }
}