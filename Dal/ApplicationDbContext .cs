﻿using Microsoft.AspNet.Identity.EntityFramework;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<FloatAccount> FloatAccounts { get; set; }

        public DbSet<FloatTransaction> FloatTransactions { get; set; }

        public DbSet<ExpenditureType> ExpenditureTypes { get; set; }

        public DbSet<ExpenditureTransaction> ExpenditureTransactions { get; set; }
        public DbSet<ActionLog> ActionLogs { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<AgentDetails> AgentDetails { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<TransactionStatus> TransactionStatus { get; set; }
    }
}
